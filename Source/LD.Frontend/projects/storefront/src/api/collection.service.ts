import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Collection, CollectionProduct, CollectionType, FilterConfigDto, Language, SearchResult, Value } from "./models";

@Injectable({ providedIn: "root" })
export class CollectionService {
    constructor(private readonly httpClient: HttpClient) {}

    public getQuantities(reference: string): Promise<{ [id: number]: number }> {
        return this.httpClient.get<{ [id: number]: number }>(`/api/collection/${reference}/quantities`).toPromise();
    }

    public getProducts(reference: string): Promise<CollectionProduct[]> {
        return this.httpClient.get<CollectionProduct[]>(`/api/collection/${reference}/products`).toPromise();
    }

    public getAllCollections(reference: string): Promise<Collection[]> {
        return this.httpClient.get<Collection[]>(`/api/collection/${reference}/products`).toPromise();
    }

    public getCollection(reference: string): Promise<Collection> {
        return this.httpClient.get<Collection>(`/api/collection/${reference}`).toPromise();
    }

    public getCollectionList(reference: string): Promise<Collection[]> {
        return this.httpClient.get<Collection[]>(`/api/collection/${reference}/list`).toPromise();
    }

    public create(type: CollectionType): Promise<{ id: string }> {
        return this.httpClient.post<{ id: string }>(`/api/collection/create/${type}`, {}).toPromise();
    }

    public confirm(reference: string): Promise<boolean> {
        return this.httpClient
            .get<Value<boolean>>(`/api/collection/${reference}/confirm`)
            .toPromise()
            .then((result) => result.value);
    }

    public addItem(reference: string, productId: number): Promise<number> {
        return this.httpClient
            .post<Value<number>>(`/api/collection/add/${reference}/${productId}`, {})
            .toPromise()
            .then((result) => result.value);
    }

    public alterQuantity(reference: string, productId: number, quantity: number): Promise<number> {
        return this.httpClient
            .post<Value<number>>(`/api/collection/alter/${reference}/${productId}/${quantity}`, {})
            .toPromise()
            .then((result) => result.value);
    }

    public removeItem(reference: string, productId: number): Promise<void> {
        return this.httpClient.delete<void>(`/api/collection/remove/${reference}/${productId}`, {}).toPromise();
    }

    public getCollectionFilters(collectionReference: string, category: string): Promise<FilterConfigDto> {
        if (category) category = `?category=${category}`;
        else category = "";
        return this.httpClient.get<FilterConfigDto>(`/api/collection/${Language.Dutch}/${collectionReference}/filters${category}`).toPromise();
    }

    public search(
        collectionReference: string,
        page: number = 1,
        category: string,
        search: string,
        rating: number,
        priceMin: number,
        priceMax: number,
        color: string
    ): Promise<SearchResult> {
        var data = { page: page, category: category, search: search, rating: rating, priceMin: priceMin, priceMax: priceMax, color: color };
        return this.httpClient.post<SearchResult>(`/api/collection/${Language.Dutch}/${collectionReference}/search`, data).toPromise();
    }
}
