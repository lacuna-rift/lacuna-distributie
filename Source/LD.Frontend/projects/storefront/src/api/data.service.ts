// Packages
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";


@Injectable({ providedIn: "root" })
export class DataService {
    constructor(private readonly httpClient: HttpClient) {}

    getClaimableOrders(): Promise<ClaimableOrderDto[]> {
        return this.httpClient.get<ClaimableOrderDto[]>(`~/api/order/claimable`).toPromise();
    }
}
