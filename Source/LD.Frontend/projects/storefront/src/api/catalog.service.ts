import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FHCachedResolver } from './helpers/FHCachedResolver';
import { FilterConfigDto, Language, Product, ProductDetail, ProductRecommendedCollectionDetail, ProductRecommendedDetail, ProductVariants, SearchResult, Slug } from "./models";

@Injectable({ providedIn: "root" })
export class CatalogService {
    constructor(private readonly httpClient: HttpClient) {}

    public getProduct(productId: number): Promise<ProductDetail> {
        return this.httpClient.get<ProductDetail>(`/api/catalog/${Language.Dutch}/${productId}`).toPromise();
    }

    public getProductVariants(productId: number): Promise<ProductVariants> {
        return this.httpClient.get<ProductVariants>(`/api/catalog/${Language.Dutch}/${productId}/variants`).toPromise();
    }

    public getProductsNewArrivals(): Promise<Product[]> {
        return this.httpClient.get<Product[]>(`/api/catalog/${Language.Dutch}/newArrivals`).toPromise();
    }

    public getSections(): Promise<Slug[]> {
        return this.httpClient.get<Slug[]>(`/api/catalog/${Language.Dutch}/sections`).toPromise();
    }

    public getCategories(): Promise<Slug[]> {
        return this.httpClient.get<Slug[]>(`/api/catalog/${Language.Dutch}/categories`).toPromise();
    }

    public getFilters(section: string, category: string): Promise<FilterConfigDto> {
        if (category) category = `?category=${category}`;
        else category = "";
        return this.httpClient.get<FilterConfigDto>(`/api/catalog/${Language.Dutch}/${section}/filters${category}`).toPromise();
    }

    public search(section: string, page: number = 1, category: string, search: string, rating: number, priceMin: number, priceMax: number, color: string): Promise<SearchResult> {
        var data = { page: page, category: category, search: search, rating: rating, priceMin: priceMin, priceMax: priceMax,color:color };
        return this.httpClient.post<SearchResult>(`/api/catalog/${Language.Dutch}/${section}/search`, data).toPromise();
    }

    public searchByPhrase(phrase: string, maxResults: number = 10): Promise<Product[]> {
        phrase = encodeURI(phrase);
        return this.httpClient.get<Product[]>(`/api/catalog/${Language.Dutch}/search-by-phrase?limit=${maxResults}&phrase=${phrase}`).toPromise();
    }
}

// ======================================>
// ======================================>
// ====> Resolvers
// ======================================>
// ======================================>
@Injectable({ providedIn: "root" })
export class CatalogNewArrivalsProductsResolver extends FHCachedResolver<Product[]> {
    private static cachedData:Product[];

    constructor(private readonly service: CatalogService) {
        super();
    }

    protected setCacheData(data: Product[]): void {
        CatalogNewArrivalsProductsResolver.cachedData = data;
    }

    protected getCacheData(): Product[] {
        return CatalogNewArrivalsProductsResolver.cachedData;
    }

    protected getFreshData(): Promise<Product[]> {
        return this.service.getProductsNewArrivals();
    }
}

@Injectable({ providedIn: "root" })
export class CatalogSectionsResolver extends FHCachedResolver<Slug[]> {
    private static cachedData:Slug[];

    constructor(private readonly service: CatalogService) {
        super();
    }

    protected setCacheData(data: Slug[]): void {
        CatalogSectionsResolver.cachedData = data;
    }

    protected getCacheData(): Slug[] {
        return CatalogSectionsResolver.cachedData;
    }

    protected getFreshData(): Promise<Slug[]> {
        return this.service.getSections();
    }
}

@Injectable({ providedIn: "root" })
export class CatalogCategoriesResolver extends FHCachedResolver<Slug[]> {
    private static cachedData:Slug[];

    constructor(private readonly service: CatalogService) {
        super();
    }

    protected setCacheData(data: Slug[]): void {
        CatalogCategoriesResolver.cachedData = data;
    }

    protected getCacheData(): Slug[] {
        return CatalogCategoriesResolver.cachedData;
    }

    protected getFreshData(): Promise<Slug[]> {
        return this.service.getCategories();
    }
}
