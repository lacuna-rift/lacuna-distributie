export class AuthenticationTokenDto {
    authToken: string;
    refreshAvailableFrom: Date; // Date Mapped
    refreshAvailableUntil: Date; // Date Mapped
}

export class SignInDto {
    username: string;
    password: string;
}

export class SignUpDto {
    email: string;
    password: string;
}

export class Product {
    id: number;
    title: string;
    slug: string;
    thumbnail: string;

    priceCurrent: number;
    priceStandard?: number;

    tag?: string;
    tagColor?: string;

    rating?: number;
    ratingColor?: string;
    ratingCount: number;

    section: Slug;
    category: Slug;
    subcategory: Slug;
}

export class ProductDetail {
    id: number;
    title: string;
    ean: string;
    brand: string;
    description: string;
    bulletPoints: string;
    color: string;
    material: string;
    size: string;
    category: string;
    subCategory: string;
    slug: string;
    images: string[];

    priceCurrent: number;
    priceStandard?: number;

    tag?: string;
    tagColor?: string;

    rating?: number;
    ratingColor?: string;
    ratingCount: number;

    stocked: boolean;
}

export class ProductRecommendedDetail {
    section: string;
    products: ProductDetail[];
}

export class ProductRecommendedCollectionDetail {
    section: string;
    slug: string;
    description: string;
    isFeatured: boolean;
    product: ProductDetail;
}

export class CollectionProduct {
    id: number;
    stocked: boolean;
    product: Product;
}

export class Collection {
    id: number;
    reference: string;
    title: string;
    slug: string;
    thumbnail: string;
}

export class ProductVariants {
    colors: Slug[];
    sizes: string[];
    variants: ProductVariant[];
}
export class ProductVariant {
    color: Slug;
    size: string;
    slug: string;
    id: number;
    hasStock: boolean;
}

export class FilterConfigDto {
    section: string;
    categories: { [id: string]: Slug[] };
    colors: { [id: string]: Slug };
    minPrice: number;
    maxPrice: number;
}
export class Slug {
    text: string;
    slug: string;
}
export enum FilterGroupType {
    None = 0,
    Category = 1,
    Color = 2,
    Price = 3,
}

export class SearchResult {
    page: number;
    pageSize: number;
    totalPages: number;
    totalProducts: number;

    products: Product[];
}

export class ReturnRequest {
    orderReference: string;
    email: string;
    reason: string;
}

export class ContactRequest {
    name: string;
    email: string;
    message: string;
    subject: string;
}

export class Faq {
    question: string;
    answer: string;
    group: string;
}


export class Value<T> {
    value: T;
}

export enum Language {
    Default = 0,
    Dutch = 1,
    English = 2,
}

export enum CollectionType {
    None = 0,
    Wishlist = 1,
    Shoppingcart = 2,
    PublicCollection = 3,
}

export enum StorePageType {
    Store = 0,
    Collection = 1,
}
