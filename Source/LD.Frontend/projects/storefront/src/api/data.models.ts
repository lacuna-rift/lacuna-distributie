declare interface ClaimableOrderDto {
    id: number;
    timestamp: Date;
    title: string;
    pickupDate: Date;
    pickupHourStart: number;
    pickupHourEnd: number;
    deliveryDate: Date;
    deliveryHourStart: number;
    deliveryHourEnd: number;
    fromPostCode: string;
    fromHouseNumber: string;
    fromHouseNumberExtension: string;
    toPostCode: string;
    toHouseNumber: string;
    toHouseNumberExtension: string;
    volume: number;
    weight: number;
    estFuelCost: number;
    baseReward: number;
} 

declare interface QuoteItemDto {
    quantity: number;
    height: number;
    length: number;
    width: number;
    weight: number;
} 

declare interface ShipmentItemDto {
    title: string;
    imageBase64: string;
    imageType: string;
    quantity: number;
    height: number;
    length: number;
    width: number;
    weight: number;
}