export abstract class FHCache<TKey, TData> {
    constructor(
        private readonly data: {
            [key: string]: TData;
        }
    ) {}
    resolve(key: TKey): Promise<TData> {
        return new Promise<TData>(resolve => {
            if (this.getCacheData(key) == null) {
                this.getFreshData(key).then(result => {
                    this.setCacheData(key, result);
                    resolve(this.createClone(result));
                });
            } else {
                resolve(this.createClone(this.getCacheData(key)));
            }
        });
    }
    protected setCacheData(key: TKey, data: TData): void {
        this.data[key.toString()] = data;
    }
    protected getCacheData(key: TKey): TData {
        return this.data[key.toString()];
    }
    protected abstract getFreshData(key: TKey): Promise<TData>;
    private createClone(input: TData): TData {
        return JSON.parse(JSON.stringify(input));
    }
}
