import { Resolve } from "@angular/router";

export abstract class FHCachedResolver<T> implements Resolve<T> {
    resolve(): Promise<T> {
        return new Promise<T>(resolve => {
            if (this.getCacheData() == null) {
                this.getFreshData().then(result => {
                    this.setCacheData(result);
                    resolve(this.createClone(result));
                });
            } else {
                resolve(this.createClone(this.getCacheData()));
            }
        });
    }
    protected abstract setCacheData(data: T): void;
    protected abstract getCacheData(): T;
    protected abstract getFreshData(): Promise<T>;
    private createClone(input: T): T {
        return JSON.parse(JSON.stringify(input));
    }
}
