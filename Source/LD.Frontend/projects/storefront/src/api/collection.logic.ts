import { EventEmitter, Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Link } from "../../src/polyfills";
import { CollectionService } from "./collection.service";
import { Collection, CollectionProduct, CollectionType, FilterConfigDto, SearchResult } from "./models";

@Injectable({ providedIn: "root" })
export class CollectionLogic {
    static WishlistCountChanged = new EventEmitter();
    static ShoppingCartCountChanged = new EventEmitter();
    Link = Link;

    constructor(private readonly cookieService: CookieService, private readonly collectionService: CollectionService) {}
    public getQuantities(type: CollectionType): Promise<{ [id: number]: number }> {
        return new Promise<{ [id: number]: number }>((resolve, reject) => {
            this.getOrCreateCollectionReference(type).then((collectionId) => {
                this.collectionService.getQuantities(collectionId).then(
                    (result) => {
                        if (type == CollectionType.Wishlist) CollectionLogic.WishlistCountChanged.next(result);
                        if (type == CollectionType.Shoppingcart) CollectionLogic.ShoppingCartCountChanged.next(result);
                        resolve(result);
                    },
                    (error) => {
                        reject(error);
                    }
                );
            });
        });
    }

    public getProducts(type: CollectionType): Promise<CollectionProduct[]> {
        return new Promise<CollectionProduct[]>((resolve, reject) => {
            this.getOrCreateCollectionReference(type).then((collectionId) => {
                this.collectionService.getProducts(collectionId).then(
                    (result) => {
                        if (type == CollectionType.Wishlist) CollectionLogic.WishlistCountChanged.next();
                        if (type == CollectionType.Shoppingcart) CollectionLogic.ShoppingCartCountChanged.next();
                        resolve(result);
                    },
                    (error) => {
                        reject(error);
                    }
                );
            });
        });
    }

    public getFeaturedProducts(): Promise<CollectionProduct[]> {
        return new Promise<CollectionProduct[]>((resolve, reject) => {
            this.collectionService.getProducts(Link.featuredProducts).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }

    public getFeaturedCollection(): Promise<Collection> {
        return new Promise<Collection>((resolve, reject) => {
            this.collectionService.getCollection(Link.featuredCollection).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }

    public getCollection(reference: string): Promise<Collection> {
        return new Promise<Collection>((resolve, reject) => {
            this.collectionService.getCollection(reference).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    public addItem(type: CollectionType, productId: number): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            this.getOrCreateCollectionReference(type).then((collectionId) => {
                this.collectionService.addItem(collectionId, productId).then(
                    (result) => {
                        if (type == CollectionType.Wishlist) CollectionLogic.WishlistCountChanged.next();
                        if (type == CollectionType.Shoppingcart) CollectionLogic.ShoppingCartCountChanged.next();
                        resolve(result);
                    },
                    (error) => {
                        reject(error);
                    }
                );
            });
        });
    }

    public alterQuantity(type: CollectionType, productId: number, quantity: number): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            this.getOrCreateCollectionReference(type).then((collectionId) => {
                this.collectionService.alterQuantity(collectionId, productId, quantity).then(
                    (result) => {
                        if (type == CollectionType.Wishlist) CollectionLogic.WishlistCountChanged.next();
                        if (type == CollectionType.Shoppingcart) CollectionLogic.ShoppingCartCountChanged.next();
                        resolve(result);
                    },
                    (error) => {
                        reject(error);
                    }
                );
            });
        });
    }

    public removeItem(type: CollectionType, productId: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.getOrCreateCollectionReference(type).then((collectionId) => {
                this.collectionService.removeItem(collectionId, productId).then(
                    (result) => {
                        if (type == CollectionType.Wishlist) CollectionLogic.WishlistCountChanged.next();
                        if (type == CollectionType.Shoppingcart) CollectionLogic.ShoppingCartCountChanged.next();
                        resolve(result);
                    },
                    (error) => {
                        reject(error);
                    }
                );
            });
        });
    }

    getOrCreateCollectionReference(type: CollectionType): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            var cookieName = "lacuna.collection." + CollectionType[type];
            if (this.cookieService.check(cookieName)) {
                var collectionId = this.cookieService.get(cookieName);
                if (window[`${cookieName}-${collectionId}`]) {
                    resolve(collectionId);
                } else {
                    this.collectionService.confirm(collectionId).then(
                        (success) => {
                            if (success) {
                                window[`${cookieName}-${collectionId}`] = true;
                                resolve(collectionId);
                            }else{
                                // delete it
                                this.cookieService.delete(cookieName);
                                // get a new one
                                this.getOrCreateCollectionReference(type).then(
                                    (collectionId) => {
                                        resolve(collectionId);
                                    },
                                    (error) => {
                                        reject(error);
                                    }
                                );
                            }
                        },
                        () => {
                            // delete it
                            this.cookieService.delete(cookieName);
                            // get a new one
                            this.getOrCreateCollectionReference(type).then(
                                (collectionId) => {
                                    resolve(collectionId);
                                },
                                (error) => {
                                    reject(error);
                                }
                            );
                        }
                    );
                }
            } else {
                this.collectionService.create(type).then(
                    (collectionId) => {
                        this.cookieService.set(cookieName, collectionId.id, 60 * 60 * 24 * 365);
                        resolve(collectionId.id);
                    },
                    (error) => {
                        reject(error);
                    }
                );
            }
        });
    }

    getFilters(collectionReference: string, category: string): Promise<FilterConfigDto> {
        return new Promise<FilterConfigDto>((resolve, reject) => {
            this.collectionService.getCollectionFilters(collectionReference, category).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }

    search(
        collectionReference: string,
        page: number = 1,
        category: string,
        search: string,
        rating: number,
        priceMin: number,
        priceMax: number,
        color: string
    ): Promise<SearchResult> {
        return new Promise<SearchResult>((resolve, reject) => {
            this.collectionService.search(collectionReference, page, category, search, rating, priceMin, priceMax, color).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
}

// ======================================>
// ======================================>
// ====> Resolvers
// ======================================>
// ======================================>
@Injectable({ providedIn: "root" })
export class WishlistCollectionResolver implements Resolve<CollectionProduct[]> {
    constructor(private readonly service: CollectionLogic) {}

    resolve(): Promise<CollectionProduct[]> {
        return this.service.getProducts(CollectionType.Wishlist);
    }
}

@Injectable({ providedIn: "root" })
export class ShoppingcartCollectionResolver implements Resolve<CollectionProduct[]> {
    constructor(private readonly service: CollectionLogic) {}

    resolve(): Promise<CollectionProduct[]> {
        return this.service.getProducts(CollectionType.Shoppingcart);
    }
}

@Injectable({ providedIn: "root" })
export class FeaturedProductsResolver implements Resolve<CollectionProduct[]> {
    constructor(private readonly service: CollectionLogic) {}

    resolve(): Promise<CollectionProduct[]> {
        return this.service.getFeaturedProducts();
    }
}

@Injectable({ providedIn: "root" })
export class FeaturedCollectionResolver implements Resolve<Collection> {
    constructor(private readonly service: CollectionLogic) {}

    resolve(): Promise<Collection> {
        return this.service.getFeaturedCollection();
    }
}
