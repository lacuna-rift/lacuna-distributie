import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";

import { AuthenticationService } from "../authentication.service";

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
    constructor(private readonly router: Router, private readonly authenticationService: AuthenticationService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const currentToken = this.authenticationService.currentToken;
            if (this.authenticationService.isValid(currentToken)) {
                resolve(true);
            } else {
                if (this.authenticationService.canRefresh(currentToken)) {
                    // New Token, is it valid?
                    this.authenticationService
                        .refreshToken()
                        .then(() => {
                            resolve(true);
                        })
                        .catch(() => {
                            // No? Get lost
                            this.goBackToSignIn(state, resolve);
                        });
                } else {
                    this.authenticationService
                        .tryLoadFromCookie()
                        .then(() => {
                            resolve(true);
                        })
                        .catch(() => {
                            // No? Get lost
                            this.goBackToSignIn(state, resolve);
                        });
                }
            }
        });
    }

    goBackToSignIn(state: RouterStateSnapshot, resolve: (arg: boolean) => void): void {
        this.router.navigate(["/login"], { queryParams: { returnUrl: state.url.substr(1) } });
        resolve(false);
    }
}
