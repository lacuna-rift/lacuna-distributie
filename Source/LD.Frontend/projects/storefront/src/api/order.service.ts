import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { CollectionLogic } from "./collection.logic";
import { CollectionType, Value } from "./models";

@Injectable({ providedIn: "root" })
export class OrderService {
    constructor(private readonly httpClient: HttpClient, private readonly collectionLogic: CollectionLogic) {}

    public getQuote(): Promise<OrderQuote> {
        return new Promise<OrderQuote>((resolve, reject) => {
            this.collectionLogic.getOrCreateCollectionReference(CollectionType.Shoppingcart).then((collectionId) => {
                this.httpClient
                    .get<OrderQuote>(`/api/order/${collectionId}/quote`)
                    .toPromise()
                    .then(
                        (result) => {
                            resolve(result);
                        },
                        (error) => {
                            reject(error);
                        }
                    );
            });
        });
    }

    public getOrderStatus(orderId: string): Promise<OrderStatus> {
        return new Promise<OrderStatus>((resolve, reject) => {
            this.httpClient
                .get<OrderStatus>(`/api/order/status/${orderId}`)
                .toPromise()
                .then(
                    (result) => {
                        resolve(result);
                    },
                    (error) => {
                        reject(error);
                    }
                );
        });
    }

    public proceedToPayment(data: OrderRequest): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.collectionLogic.getOrCreateCollectionReference(CollectionType.Shoppingcart).then((collectionId) => {
                console.log(data);
                this.httpClient
                    .post<Value<string>>(`/api/order/${collectionId}/finalize`, data)
                    .toPromise()
                    .then(
                        (result) => {
                            resolve(result.value);
                        },
                        (error) => {
                            reject(error);
                        }
                    );
            });
        });
    }
}

// ======================================>
// ======================================>
// ====> MODELS
// ======================================>
// ======================================>
export enum PaymentMethod {
    None = 0,
    IDeal = 1,
    Paypal = 2,
}

export class OrderRequest {
    paymentMethod: PaymentMethod = PaymentMethod.IDeal;
    firstname: string;
    lastname: string;
    businessName: string;
    telephoneNumber: string;
    emailaddress: string;
    postcode: string;
    houseNumber: string;
    street: string;
    city: string;
    expectedTotal: number;
}

export class OrderQuote {
    itemTotal: number;
    itemTotalIncl: number;
    shipping: number;
    paymentMethod: number;
    btw: number;
    taxPercent: number;
    totalIncl: number;
}

export enum OrderStatus {
    Open = 0,
    Canceled = 1,
    Pending = 2,
    Authorized = 3,
    Expired = 4,
    Failed = 5,
    Paid = 6,
}

// ======================================>
// ======================================>
// ====> Resolvers
// ======================================>
// ======================================>
@Injectable({ providedIn: "root" })
export class OrderQuoteResolver implements Resolve<OrderQuote> {
    constructor(private readonly service: OrderService) {}

    resolve(): Promise<OrderQuote> {
        return this.service.getQuote();
    }
}
