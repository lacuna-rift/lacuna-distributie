// Packages
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

// Service Specification

@Injectable({ providedIn: "root" })
export class AccountService {

    public static LoginRoute: string = "/signup";
    public static ApiRoute: string = "~";

    constructor(readonly httpClient: HttpClient) {}

    signup(email: string, password: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
        const dto = new SignUpDto();
            dto.username = email;
            dto.password = password;
            this.httpClient
                .post<boolean>(`/api/account/signup`, dto)
                .toPromise()
                .then((result) => {
                    resolve(result);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    recoverPassword(email: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
        const dto = new RecoverPasswordDto();
            dto.username = email;
            this.httpClient
                .post<boolean>(`/api/account/recover-password`, dto)
                .toPromise()
                .then((result) => {
                    resolve(result);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    resetPassword(secret: string, password: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
        const dto = new ResetPasswordDto();
            dto.secret = secret;
            dto.newPassword = password;
            this.httpClient
                .post<boolean>(`/api/account/reset-password`, dto)
                .toPromise()
                .then((result) => {
                    resolve(result);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

export class AccountDto {
    id: number;
    CreationTimestamp: Date;
    DisplayName: string;
    FirstName: string;
    Insertion: string;
    LastName: string;
    Email: string;
}

export class SignUpDto {
    username: string;
    password: string;
}

export class RecoverPasswordDto {
    username: string;
}

export class ResetPasswordDto {
    secret: string;
    newPassword: string;
}
