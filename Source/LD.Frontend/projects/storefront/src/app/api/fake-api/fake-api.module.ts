import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountApi, BlogApi, CountriesApi, ShopApi, VehicleApi } from '../base';
import { FakeBlogApi } from './fake-blog.api';
import { FakeCountriesApi } from './fake-countries.api';
import { FakeShopApi } from './fake-shop.api';
import { FakeVehicleApi } from './fake-vehicle.api';
import { LacunaAccountApi } from '../lacuna-api/lacuna-account.api';

@NgModule({
    imports: [
        CommonModule,
    ],
    providers: [
        {provide: AccountApi, useClass: LacunaAccountApi},
        {provide: BlogApi, useClass: FakeBlogApi},
        {provide: CountriesApi, useClass: FakeCountriesApi},
        {provide: ShopApi, useClass: FakeShopApi},
        {provide: VehicleApi, useClass: FakeVehicleApi},
    ],
})
export class FakeApiModule { }
