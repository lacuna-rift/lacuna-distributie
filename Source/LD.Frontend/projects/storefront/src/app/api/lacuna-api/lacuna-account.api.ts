import { Injectable } from '@angular/core';
import {
    AccountApi,
    EditProfileData,
    EditAddressData,
    GetOrdersListOptions,
} from '../base';
import { User } from '../../interfaces/user';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { Address } from '../../interfaces/address';
import { OrdersList } from '../../interfaces/list';
import { Order } from '../../interfaces/order';
import {
    accountChangePassword,
    accountEditProfile,
    accountSignIn,
    accountSignOut,
    accountSignUp,
    addAddress,
    delAddress,
    editAddress,
    getAddress,
    getAddresses,
    getDefaultAddress,
    getOrderById,
    getOrderByToken,
    getOrdersList,
} from '../../../fake-server/endpoints';
import { tap } from 'rxjs/operators';
import { delayResponse } from '../../../fake-server/utils';
import { AuthenticationService } from '../../../api/authentication.service';
import { AccountService } from '../../../api/account.service';

@Injectable()
export class LacunaAccountApi extends AccountApi {
    private userSubject: BehaviorSubject<User | null>;

    get user(): User | null {
        return this.userSubject.value;
    }

    readonly user$: Observable<User | null>;

    constructor(
        private readonly authenticationService: AuthenticationService,
        private readonly accountService: AccountService
    ) {
        super();

        const storedUser = localStorage.getItem('user');

        this.userSubject = new BehaviorSubject<User | null>(
            storedUser ? JSON.parse(storedUser) : null
        );
        this.user$ = this.userSubject.asObservable();
    }

    signIn(email: string, password: string): Observable<User> {
        // return accountSignIn(email, password).pipe(
        //     tap(user => this.setUser(user)),
        // );
        var test = this.authenticationService.authenticate(email, password);
        var user: User = {
            email: 'asd@asd.com',
            phone: '123 456 789',
            firstName: 'John',
            lastName: 'Doe',
            avatar:
                '//www.gravatar.com/avatar/bde30b7dd579b3c9773f80132523b4c3?d=mp&s=88',
        };

        return delayResponse(of(user));
    }

    signUp(email: string, password: string): Observable<void> {
        var result = this.accountService.signup(email, password);

        if (result) {
            this.signIn(email, password);
        }
        return delayResponse(of());
    }

    signOut(): Observable<void> {
        return accountSignOut().pipe(tap(() => this.setUser(null)));
    }

    editProfile(data: EditProfileData): Observable<User> {
        return accountEditProfile(data).pipe(tap((user) => this.setUser(user)));
    }

    changePassword(oldPassword: string, newPassword: string): Observable<void> {
        //return accountChangePassword(oldPassword, newPassword);
        return delayResponse(of());
    }

    addAddress(data: EditAddressData): Observable<Address> {
        return addAddress(data);
    }

    editAddress(addressId: number, data: EditAddressData): Observable<Address> {
        return editAddress(addressId, data);
    }

    delAddress(addressId: number): Observable<void> {
        return delAddress(addressId);
    }

    getDefaultAddress(): Observable<Address | null> {
        return getDefaultAddress();
    }

    getAddress(addressId: number): Observable<Address> {
        return getAddress(addressId);
    }

    getAddresses(): Observable<Address[]> {
        return getAddresses();
    }

    getOrdersList(options?: GetOrdersListOptions): Observable<OrdersList> {
        return getOrdersList(options);
    }

    getOrderById(id: number): Observable<Order> {
        return getOrderById(id);
    }

    getOrderByToken(token: string): Observable<Order> {
        return getOrderByToken(token);
    }

    private setUser(user: User): void {
        this.userSubject.next(user);

        localStorage.setItem('user', JSON.stringify(user));
    }
}
