/**
 * This file includes polyfills needed by Angular and is loaded before the app.
 * You can add your own extra polyfills to this file.
 *
 * This file is divided into 2 sections:
 *   1. Browser polyfills. These are applied before loading ZoneJS and are sorted by browsers.
 *   2. Application imports. Files imported after ZoneJS that should be loaded before your main
 *      file.
 *
 * The current setup is for so-called "evergreen" browsers; the last versions of browsers that
 * automatically update themselves. This includes Safari >= 10, Chrome >= 55 (including Opera),
 * Edge >= 13 on the desktop, and iOS 10 and Chrome on mobile.
 *
 * Learn more in https://angular.io/guide/browser-support
 */

/***************************************************************************************************
 * BROWSER POLYFILLS
 */

/** IE10 and IE11 requires the following for NgClass support on SVG elements */
// import 'classlist.js';  // Run `npm install --save classlist.js`.

/**
 * Web Animations `@angular/platform-browser/animations`
 * Only required if AnimationBuilder is used within the application and using IE/Edge or Safari.
 * Standard animation support in Angular DOES NOT require any polyfills (as of Angular 6.0).
 */
// import 'web-animations-js';  // Run `npm install --save web-animations-js`.

/**
 * By default, zone.js will patch all possible macroTask and DomEvents
 * user can disable parts of macroTask/DomEvents patch by setting following flags
 * because those flags need to be set before `zone.js` being loaded, and webpack
 * will put import in the top of bundle, so user need to create a separate file
 * in this directory (for example: zone-flags.ts), and put the following flags
 * into that file, and then add the following code before importing zone.js.
 * import './zone-flags';
 *
 * The flags allowed in zone-flags.ts are listed here.
 *
 * The following flags will work for all browsers.
 *
 * (window as any).__Zone_disable_requestAnimationFrame = true; // disable patch requestAnimationFrame
 * (window as any).__Zone_disable_on_property = true; // disable patch onProperty such as onclick
 * (window as any).__zone_symbol__UNPATCHED_EVENTS = ['scroll', 'mousemove']; // disable patch specified eventNames
 *
 *  in IE/Edge developer tools, the addEventListener will also be wrapped by zone.js
 *  with the following flag, it will bypass `zone.js` patch for IE/Edge
 *
 *  (window as any).__Zone_enable_cross_context_check = true;
 *
 */

/***************************************************************************************************
 * Zone JS is required by default for Angular itself.
 */
import 'zone.js/dist/zone';  // Included with Angular CLI.


/***************************************************************************************************
 * APPLICATION IMPORTS
 */
export abstract class Link {
    static readonly login = "/login";                               // Needs language check, Needs form validation (with disabled button), needs faliure feedback, needs brute force protection
    static readonly signup = "/register";                           // Needs language check, Needs form validation (with disabled button), needs faliure feedback
    static readonly recoverPassword = "/wachtwoord-vergeten";       // Needs form validation (with disabled button), needs faliure feedback
    static readonly checkout = "/afrekenen";                        // Needs form validation (with disabled button), needs faliure feedback
    static readonly customerService = "/klantenservice";            // Needs styling, needs submit implementation
    static readonly account = "/account";                           // unknown
    static readonly wishlist = "/verlanglijstje";                   // unknown
    static readonly shoppingCart = "/winkelwagen";                  // unknown
    static readonly privacyPolicy = "/privacybeleid";               // Needs styling, Needs ts values filled in, Fix links and downloads
    static readonly termsAndConditions = "/algemene-voorwaarden";   // Needs styling, Needs ts values filled in, Fix links and downloads
    static readonly faq = "/veelgestelde-vragen";                   // Needs styling, Review faqs, Fix up links like klantenservice
    static readonly contact = "/contact";                           // unknown
    static readonly returns = "/ruilen-en-retourneren";             // unknown
    static readonly orders = "/bestellingen";                       // unknown
    static readonly deliveryTimes = "/leveringsinformatie";         // Review spelling & shipping price
    static readonly home = "/";                                     // unknown

    static readonly featuredCollection = "herfst-collectie-2020";
    static readonly featuredProducts = "featured-products";

    static readonly detailsCompanyName: string = "Lacuna Detailhandel";
    static readonly detailsWebsite: string = "www.stuntvandeweek.nl";
    static readonly detailsEmail: string = "support@lacuna-detailhandel.com";
    static readonly detailsKvk: string = "74796321";
    static readonly detailsBtw: string = "NL860029554B01";
    static readonly detailsIban: string = "NL123";
    static readonly detailsAddressStreet: string = "Reestraat 25";
    static readonly detailsAddressPostalCode: string = "6562 LJ";
    static readonly detailsAddressCity: string = "Groesbeek";
    static readonly detailsAddressCountry: string = "Nederland";
    static readonly detailsPhone: string = "+31 617 106 110";
    static readonly detailsShippingCostNL = "€ 4.79";

    static toProduct(product: { slug: string; id: number }): any[] {
        return ["/", "p", product.slug, product.id];
    }

    static toStore(section: string, category?: string, thePage?: number): any[] {
        return ["/", "s", section, category, thePage].filter((o) => o);
    }

    // the collection should be one from the PublicCollections class!
    static toCollection(collection: string, category?: string, thePage?: number): any[] {
        return ["/", "c", collection, category, thePage].filter((o) => o);
    }
}
