﻿using System.Linq;
using Lacuna.Core.Extensions;
using LD.Source;
using LD.Source.Amazon;
using Microsoft.Extensions.DependencyInjection;

namespace LD.Tools.MetaData
{
    class Program
    {
        private const string FilePath = @"c:\temp\";

        static void Main(string[] args)
        {
            var builder = new ServiceCollection();
            builder.RegisterModule<AmazonSourceModule>();
            var provider = builder.BuildServiceProvider();

            var datasource = provider.GetServices<IManifestDatasource>().Single(o => o.Vendor == AmazonConstants.Vendor);
            var metadataDatasource = provider.GetServices<IManifestMetaDatasource>().Single(o => o.Vendor == AmazonConstants.Vendor);

            var items = datasource.GetItems("AZPL_CR_BP1_20200727stl").ToArray();

            var grouped = items
                .GroupBy(o => o.Meta.ASIN)
                .OrderByDescending(o => o.Count())
                .ToDictionary(o => o.Key, o => new { Retail = o.First().Meta.SuggestedRetailValue, Items = o.ToArray() });

            foreach (var key in grouped.Keys)
            {
                var item = grouped[key].Items.FirstOrDefault();
            }
        }
    }
}
