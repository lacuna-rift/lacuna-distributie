﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using Lacuna.Core.Extensions;
using LD.Source;
using LD.Source.Amazon;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace LD.Tools.ShipmentScannner
{
    public class RegistryContext
    {
        public string ShipmentId { get; set; }
        public string PalletId { get; set; }
        public string Barcode { get; set; }
        public int ExternalRating { get; set; }
        public int InternalRating { get; set; }
        public int ProductRating { get; set; }
        public int AccessoriesRating { get; set; }
    }

    class Program
    {
        public static List<string> KnownBarcodes { get; set; } = new List<string>();
        private static readonly ConcurrentDictionary<string, ScannerItemInfo> InfoCache = new ConcurrentDictionary<string, ScannerItemInfo>();
        private const string OutputFile = "./scan_output.txt";
        private const string OutputSeperator = "\t";

        private const bool EnableProcessQuality = true;

        public static readonly RegistryContext Context = new RegistryContext();

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            //if (File.Exists(OutputFile))
            //{
            //    KnownBarcodes = File.ReadAllLines(OutputFile).Select(l => l.Split(OutputSeperator)[3]).ToList();
            //    Console.Title = $"Loaded {KnownBarcodes.Count} Barcodes";
            //    var errors = 0;
            //    for (var i = 0; i < KnownBarcodes.Count; i++)
            //    {
            //        var lpn = KnownBarcodes[i];
            //        try
            //        {
            //            using var wc = new WebClient();
            //            var json = wc.DownloadString($"https://localhost:8556/api/scanner/get/lpn/{lpn}");
            //            var meta = JsonConvert.DeserializeObject<ScannerItemInfo>(json);

            //            var title = meta.Meta.Title.Substring(0, Math.Min(meta.Meta.Title.Length, 150));
            //            if (title.Length != meta.Meta.Title.Length)
            //                title += " ...";
            //            Console.WriteLine($"[{i} / {KnownBarcodes.Count}] \tLoaded Barcode {lpn} > {title}");
            //            InfoCache[lpn] = meta;
            //        }
            //        catch
            //        {
            //            Console.ForegroundColor = ConsoleColor.Red;
            //            Console.WriteLine($"[{i} / {KnownBarcodes.Count}] \tFailed to Loaded Barcode {lpn}");
            //            Console.ForegroundColor = ConsoleColor.Gray;
            //            errors++;
            //        }
            //    }

            //    if (errors > 0)
            //    {
            //        Console.ForegroundColor = ConsoleColor.Red;
            //        Console.WriteLine($"There were {errors} errors, press <ENTER> to continue...");
            //        Console.ForegroundColor = ConsoleColor.Gray;
            //        Console.ReadLine();
            //    }
            //}

            Context.ShipmentId = "x";
            Context.PalletId = "x";

            //FillShipmentId();
            while (true)
            {
                //FillPalletId();
                ProcessItem();
                if (EnableProcessQuality)
                {
                    ProcessQuality();
                }
                else
                {
                    Context.AccessoriesRating = 5;
                    Context.ExternalRating = 5;
                    Context.InternalRating = 5;
                    Context.ProductRating = 5;
                }
                FinalizeItem();
            }

        }


        private static bool ProcessQuality()
        {
            var externalRating = AskKeyQuestion("Exterior Packaging Rating (1-5 or S=Sealed): ", "12345XS");
            if (externalRating == 'X') return false;
            if (externalRating == 'S')
            {
                Context.ExternalRating = 5;
                Context.InternalRating = 5;
                Context.ProductRating = 5;
                Context.AccessoriesRating = 5;
            }
            else
            {
                Context.ExternalRating = int.Parse(externalRating.ToString());

                var internalRating = AskKeyQuestion("Internal Packaging Rating (1-5): ", "12345");
                if (internalRating == 'X') return false;
                Context.InternalRating = int.Parse(internalRating.ToString());

                var productRating = AskKeyQuestion("Product Rating (1-5): ", "12345");
                if (productRating == 'X') return false;
                Context.ProductRating = int.Parse(productRating.ToString());

                var accessoriesRating = AskKeyQuestion("Accessories Rating (1-5): ", "12345");
                if (accessoriesRating == 'X') return false;
                Context.AccessoriesRating = int.Parse(accessoriesRating.ToString());
            }
            return true;
        }

        private static void PrintCurrentData()
        {
            Console.Clear();
            Console.WriteLine($"=============================================================");
            Console.WriteLine($"| ShipmentId                : {Context.ShipmentId}");
            Console.WriteLine($"| PalletId                  : {Context.PalletId}");

            if (Context.Barcode?.Length == 14)
            {
                Console.WriteLine($"| Barcode                   : {Context.Barcode}");

                var meta = InfoCache.GetOrAdd(Context.Barcode, lpn =>
                {
                    return null;
                    try
                    {
                        using var wc = new WebClient();
                        var json = wc.DownloadString($"https://localhost:8556/api/scanner/get/lpn/{lpn}");
                        return JsonConvert.DeserializeObject<ScannerItemInfo>(json);
                    }
                    catch (WebException e)
                    {
                        return null;
                    }
                });

                Console.Write($"| Title                     : ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(meta?.Meta.Title ?? "NOT FOUND - IS THE SERVER RUNNING?");
                Console.ResetColor();
                if (meta != null)
                {
                    Console.Write($"| Reported Condition        : ");
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.WriteLine(meta?.ReportedCondition);
                    Console.ResetColor();

                    Console.Write($"| Estimated Value           : ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(Math.Max(meta.Meta.LastKnownPrice, meta.Meta.SuggestedRetailValue).ToString("N2"));
                    Console.ResetColor();
                }
            }
            Console.WriteLine($"| Exterior Packaging Rating : {Context.ExternalRating}");
            Console.WriteLine($"| Internal Packaging Rating : {Context.InternalRating}");
            Console.WriteLine($"| Product Rating            : {Context.ProductRating}");
            Console.WriteLine($"| Accessories Rating        : {Context.AccessoriesRating}");
            Console.WriteLine($"=============================================================");
        }

        private static void FinalizeItem()
        {
            if (!KnownBarcodes.Contains(Context.Barcode ?? ""))
            {
                KnownBarcodes.Add(Context.Barcode);
                Console.Title = $"BARCODE {Context.Barcode} Saved";
                File.AppendAllLines(OutputFile, new[] { $"{DateTime.UtcNow:o}{OutputSeperator}" +
                                                        $"{Context.ShipmentId}{OutputSeperator}" +
                                                        $"{Context.PalletId}{OutputSeperator}" +
                                                        $"{Context.Barcode}{OutputSeperator}" +
                                                        $"{Context.ExternalRating}{OutputSeperator}" +
                                                        $"{Context.InternalRating}{OutputSeperator}" +
                                                        $"{Context.ProductRating}{OutputSeperator}" +
                                                        $"{Context.AccessoriesRating}"

                });
            }
            else
            {
                Console.Beep(1200, 100);
                Console.Beep(1200, 100);
                Console.Beep(1200, 100);
                Console.Title = $"ERROR! BARCODE {Context.Barcode} NOT SAVED, ALREADY KNOWN!";
            }
        }

        private static bool ProcessItem()
        {
            PrintCurrentData();
            while (true)
            {
                var barcode = AskQuestion("Please scan the barcode of the item: ");
                if (barcode.StartsWith("LPN") && barcode.Length == 14) // LPN
                {
                    Context.Barcode = barcode;
                    Console.WriteLine("LPN Barcode found");
                    Console.Beep(900, 100);
                    break;
                }
                //if (barcode.Length == 13) // EAN
                //{
                //    Context.Barcode = barcode;
                //    Console.WriteLine("EAN Barcode found");
                //    break;
                //}
                //else if (barcode.Length == 12) // UPC
                //{
                //    Context.Barcode = barcode;
                //    Console.WriteLine("UPC Barcode found");
                //    break;
                //}
                else
                {
                    Console.WriteLine($"<! Invalid Barcode format ({barcode.Length} digits) !>");
                }
            }

            return true;
        }

        private static void FillPalletId()
        {
            while (true)
            {
                var action = AskKeyQuestion("What would you like to do next?\n" +
                                            "[N] Register NON-pallet item\n" +
                                            "[P] Register a pallet item\n" +
                                            (string.IsNullOrWhiteSpace(Context.PalletId) ? "" : $"[C] Continue with previous pallet... ({Context.PalletId})"),
                                            "NPC");
                if (action == 'N')
                {
                    Context.PalletId = "NONE";
                    break;
                }
                if (action == 'P')
                {
                    Context.PalletId = AskQuestion("PalletId: ");
                    break;
                }
                if (action == 'C')
                {
                    break;
                }
            }

            Console.Title = $"Shipment : {Context.ShipmentId} - Pallet :{Context.PalletId}";
        }

        private static void StateInvalidOption()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("<! Invalid Option !>");
            Console.ResetColor();
        }

        private static void FillShipmentId()
        {
            Context.ShipmentId = AskQuestion("ShipmentId: ");
            Console.Title = $"Shipment : {Context.ShipmentId}";
        }

        private static char AskKeyQuestion(string question, string validCharacters) => AskKeyQuestion(question, validCharacters.ToCharArray());
        private static char AskKeyQuestion(string question, params char[] validCharacters)
        {
            while (true)
            {
                Console.WriteLine(question.Trim());
                Console.Write("> ");
                var c = char.ToUpper(Console.ReadKey(true).KeyChar);
                if (c == '\r') c = char.ToUpper(Console.ReadKey(true).KeyChar);
                Console.WriteLine(c);
                foreach (var validCharacter in validCharacters)
                {
                    if (c == validCharacter)
                        return c;
                }
                StateInvalidOption();
            }
        }

        private static string AskQuestion(string question)
        {
            Console.WriteLine(question.Trim());
            Console.Write("> ");
            string result;
            do
            {
                result = Console.ReadLine();
            } while (result.Trim().Length == 0);
            return result;
        }
    }
}

public class ScannerItemInfo
{
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("reportedCondition")]
    public string ReportedCondition { get; set; }

    [JsonProperty("meta")]
    public ScannerItemInfoMeta Meta { get; set; }
}
public class ScannerItemInfoMeta
{
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("vendor")]
    public string Vendor { get; set; }

    [JsonProperty("languageCode")]
    public string LanguageCode { get; set; }

    [JsonProperty("timestamp")]
    public DateTime Timestamp { get; set; }

    [JsonProperty("department")]
    public string Department { get; set; }

    [JsonProperty("category")]
    public string Category { get; set; }

    [JsonProperty("subCategory")]
    public string SubCategory { get; set; }

    [JsonProperty("asin")]
    public string Asin { get; set; }

    [JsonProperty("ean")]
    public string Ean { get; set; }

    [JsonProperty("suggestedRetailValue")]
    public double SuggestedRetailValue { get; set; }

    [JsonProperty("lastKnownPrice")]
    public double LastKnownPrice { get; set; }

    [JsonProperty("weight")]
    public int Weight { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("features")]
    public string Features { get; set; }

    [JsonProperty("lastSourceUrl")]
    public string LastSourceUrl { get; set; }
}