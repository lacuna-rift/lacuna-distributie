namespace LD.Gateway.Models
{
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public string DescriptionHtml { get; set; }
        public string Slug { get; set; }
        public string Sku { get; set; }


        public int Stock { get; set; }
        public decimal Price { get; set; }

        // partNumber : string (became EAN)
        // compareAtPrice : number | null
        // badges? : string []

        public string Ean { get; set; }
        public ProductBadge Badges { get; set; }
        public ProductTag Tags { get; set; }
        public Category Category { get; set; }
    }
}