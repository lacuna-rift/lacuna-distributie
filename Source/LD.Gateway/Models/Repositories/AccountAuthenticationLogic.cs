using System;
using System.Threading.Tasks;
using Lacuna.Authentication.AspNetCore;
using LD.Gateway.Models.Dtos;

namespace LD.Gateway.Models.Repositories
{
    public class AccountAuthenticationLogic : IAuthenticationLogic
    {
        private readonly AccountAuthenticationRepository _accountAuthenticationRepository;
        private readonly AuthenticationCryptography _authenticationCryptography;

        public AccountAuthenticationLogic(AccountAuthenticationRepository accountAuthenticationRepository,
            AuthenticationCryptography authenticationCryptography)
        {
            _accountAuthenticationRepository = accountAuthenticationRepository;
            _authenticationCryptography = authenticationCryptography;
        }

        public Task CreateAuthenticatableIdentity(SignUpDto data)
        {
            var account = new Account
            {
                Email = data.Username,
                Password = _authenticationCryptography.Hash(data.Password),
                FirstName = "",
                Insertion = "",
                LastName = "",
                CreationTimestamp = DateTime.UtcNow,
                LastActivityTimestamp = DateTime.UtcNow
            };

            return _accountAuthenticationRepository.CreateAuthenticatableIdentity(account);
        }

        public async Task<IAuthenticatableIdentity> GetAuthenticatableIdentity(long identityId)
        {
            return await _accountAuthenticationRepository.GetAuthenticatableIdentity(identityId);
        }

        public async Task<IAuthenticatableIdentity> GetAuthenticatableIdentity(string username, string password)
        {
            return await _accountAuthenticationRepository.GetAuthenticatableIdentity(username, password);
        }

        public Task UpdateLastActivity(long identityId)
        {
            return _accountAuthenticationRepository.UpdateLastActivity(identityId);
        }

        public Task Commit()
        {
            return _accountAuthenticationRepository.Commit();
        }
    }
}