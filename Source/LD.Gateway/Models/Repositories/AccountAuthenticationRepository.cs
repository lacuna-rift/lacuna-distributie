using System;
using System.Linq;
using System.Threading.Tasks;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Persistance.Interfaces;
using NHibernate.Linq;

namespace LD.Gateway.Models.Repositories
{
    public class AccountAuthenticationRepository
    {
        private readonly IUnitOfWork _db;

        public AccountAuthenticationRepository(IUnitOfWork db)
        {
            _db = db;
        }

        public Task CreateAuthenticatableIdentity(Account account)
        {
            return _db.InsertAsync(account);
        }

        public Task<Account> GetAuthenticatableIdentity(long identityId)
        {
            return _db.Query<Account>()
                .Where(o => o.Id == identityId)
                .SingleOrDefaultAsync();
        }

        public Task<Account> GetAuthenticatableIdentity(string username, string password)
        {
            return _db.Query<Account>()
                .Where(o => o.Email == username)
                .Where(o => o.Password == password)
                .SingleOrDefaultAsync();
        }

        public Task UpdateLastActivity(long identityId)
        {
            return _db.Query<Account>()
                .Where(o => o.Id == identityId)
                .UpdateSet(o => o.LastActivityTimestamp, DateTime.UtcNow)
                .ExecuteUpdateAsync();
        }

        public Task Commit()
        {
            _db.Commit();
            return Task.CompletedTask;
        }
    }
}