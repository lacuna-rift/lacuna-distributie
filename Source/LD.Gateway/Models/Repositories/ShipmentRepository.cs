﻿using System.Threading.Tasks;
using Lacuna.Core.Data.Persistance.Interfaces;
using LD.Source.Models;
using NHibernate.Linq;

namespace LD.Gateway.Models.Repositories
{
    public class ShipmentRepository
    {
        private readonly IUnitOfWork _db;

        public ShipmentRepository(IUnitOfWork db)
        {
            _db = db;
        }

        public Task<Shipment> GetShipment(string reference)
        {
            return _db.Query<Shipment>().SingleOrDefaultAsync(o => o.Reference == reference);
        }

        public Task<Shipment> GetShipment(int shipmentId)
        {
            return _db.Query<Shipment>().SingleOrDefaultAsync(o => o.Id == shipmentId);
        }

        public Task<ShipmentPallet> GetPallet(string reference)
        {
            return _db.Query<ShipmentPallet>().SingleOrDefaultAsync(o => o.Reference == reference);
        }

        public Task<ShipmentPallet> GetPallet(int palletId)
        {
            return _db.Query<ShipmentPallet>().SingleOrDefaultAsync(o => o.Id == palletId);
        }

        public Task<ShipmentPalletItem> GetItem(string reference)
        {
            return _db.Query<ShipmentPalletItem>().SingleOrDefaultAsync(o => o.Reference == reference);
        }

        public Task<ShipmentPalletItem> GetItem(int itemId)
        {
            return _db.Query<ShipmentPalletItem>().SingleOrDefaultAsync(o => o.Id == itemId);
        }
    }
}