﻿using System.Linq;
using System.Threading.Tasks;
using Lacuna.Core.Data.Persistance.Interfaces;
using LD.Gateway.Models.V2;
using NHibernate.Linq;

namespace LD.Gateway.Models.Repositories
{
    public class StorageRepository
    {
        private readonly IUnitOfWork _db;

        public StorageRepository(IUnitOfWork db)
        {
            _db = db;
        }

        public Task<StorageSlot> GetSlot(byte warehouse, byte zone, byte rack, byte row, byte slot)
        {
            return _db.Query<StorageSlot>()
                .Where(o => o.Warehouse == warehouse)
                .Where(o => o.Zone == zone)
                .Where(o => o.Rack == rack)
                .Where(o => o.Row == row)
                .Where(o => o.Slot == slot)
                .SingleOrDefaultAsync();
        }
    }
}