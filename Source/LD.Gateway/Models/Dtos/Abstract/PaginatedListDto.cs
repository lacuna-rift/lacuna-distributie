﻿namespace LD.Gateway.Models.Dtos.Abstract
{
    public class PaginatedListDto<T>
    {
        /** Current page. */
        public int Page { get; set; }
        /** Items per page. */
        public int Limit { get; set; }
        /** Sorting algorithm. */
        public string Sort { get; set; }
        /** Total items in list. Not a items.length. */
        public int Total { get; set; }
        /** Total number of pages. */
        public int Pages { get; set; }
        /** Common number of the first item on the current page. */
        public int From { get; set; }
        /** Common number of the last item on the current page. */
        public int To { get; set; }

        public T[] Items { get; set; } = { };
    }
}