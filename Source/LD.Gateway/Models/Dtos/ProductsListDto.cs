﻿using LD.Gateway.Models.Dtos.Abstract;

namespace LD.Gateway.Models.Dtos
{
    public class ProductsListDto : PaginatedListDto<ProductDto>
    {
        public FilterDto[] Filters { get; set; } = { };
    }
}