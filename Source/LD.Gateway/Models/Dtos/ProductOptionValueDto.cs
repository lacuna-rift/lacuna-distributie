﻿namespace LD.Gateway.Models.Dtos
{
    public class ProductOptionValueDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public object? CustomFields { get; set; }
    }
}
