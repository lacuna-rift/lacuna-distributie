﻿using System;

namespace LD.Gateway.Models.Dtos
{
    public class ReviewDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public DateTime Date { get; set; }
        public string Author { get; set; }
        public string Avatar { get; set; }
        public int Rating { get; set; }
        public string Content { get; set; }
    }
}