﻿namespace LD.Gateway.Models.Dtos
{
    public class ProductOptionsDto
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public ProductOptionValueDto[] Values { get; set; } = { };
        public object? CustomFields { get; set; }
    }
}
