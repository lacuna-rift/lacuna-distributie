﻿namespace LD.Gateway.Models.Dtos
{
    public class ProductTypeDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public ProductTypeAttributeGroupDto[] AttributeGroups { get; set; } = { };
    }
}