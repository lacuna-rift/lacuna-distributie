﻿namespace LD.Gateway.Models.Dtos
{
    public enum ProductStock
    {
        InStock,
        OutOfStock,
        OnBackOrder
    }
}