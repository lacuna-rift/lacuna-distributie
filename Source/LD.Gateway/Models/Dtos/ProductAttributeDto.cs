﻿namespace LD.Gateway.Models.Dtos
{
    public class ProductAttributeDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public bool Featured { get; set; }
        public ProductAttributeValueDto[] Values { get; set; } = { };
    }
}