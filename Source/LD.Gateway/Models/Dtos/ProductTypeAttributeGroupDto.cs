﻿namespace LD.Gateway.Models.Dtos
{
    public class ProductTypeAttributeGroupDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public string[] Attributes { get; set; } = { };
    }
}