﻿namespace LD.Gateway.Models.Dtos
{
    public class ShopCategoryDto
    {
        public int Id { get; set; }
        public string Type { get; set; } = "shop";
        public string Layout { get; set; } = "categories";
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Image { get; set; }
        public int? Items { get; set; }
    }
}