﻿namespace LD.Gateway.Models.Dtos
{
    public class ProductAttributeValueDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
    }
}