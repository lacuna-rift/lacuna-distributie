﻿using System;

namespace LD.Gateway.Models.Dtos
{
    public class AccountDto
    {
        public long Id { get; set; }
        public DateTime CreationTimestamp { get; set; }

        public string DisplayName => FirstName + " " + (Insertion + " ").Trim() + LastName;

        public string FirstName { get; set; }
        public string Insertion { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }
    }
}