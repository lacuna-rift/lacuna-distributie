﻿using System;

namespace LD.Gateway.Models.Dtos
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Excerpt { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public string Sku { get; set; }
        public string PartNumber => Sku;
        public ProductStock Stock { get; set; }
        public decimal Price { get; set; }
        public decimal? NormalPrice { get; set; }
        public string[] Images { get; set; }
        public string[] Badges { get; set; }
        public int? Rating { get; set; }
        public int? Reviews { get; set; }
        public string Availability { get; set; }
        public string Compatibility { get; } = "all";
        public ProductBrandDto Brand { get; set; }
        public string[] Tags { get; set; }
        public ProductTypeDto Type { get; set; }
        public ShopCategoryDto[] Categories { get; set; } = { };
        public ProductAttributeDto[] Attributes { get; set; } = { };
        public ProductOptionsDto[] Options { get; set; } = {};

    }
}
