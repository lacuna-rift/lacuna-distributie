﻿using LD.Gateway.Models.Dtos.Abstract;

namespace LD.Gateway.Models.Dtos
{
    public class ReviewsListDto : PaginatedListDto<ReviewDto>
    {
    }
}