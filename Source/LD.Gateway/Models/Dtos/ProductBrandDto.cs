﻿namespace LD.Gateway.Models.Dtos
{
    public class ProductBrandDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Slug { get; set; }
        public string Country { get; set; }
    }
}