﻿using Lacuna.Core.Data.Mapping;

namespace LD.Gateway.Models.V2
{
    public class StorageSlot
    {
        public virtual int Id { get; set; }

        public virtual byte Warehouse { get; set; }
        public virtual byte Zone { get; set; }
        public virtual byte Rack { get; set; }
        public virtual byte Row { get; set; }
        public virtual byte Slot { get; set; }

        [StringLength(1000)]
        public virtual string Note { get; set; }

        public virtual byte UsableWidthCm { get; set; }
        public virtual byte UsableBreadthCm { get; set; }
        public virtual byte UsableHeightCm { get; set; }
    }
}