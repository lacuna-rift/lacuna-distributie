using System;

namespace LD.Gateway.Models
{
    public class ProductReview
    {
        public long Id { get; set; }
        public Product Product { get; set; }
        public Account Account { get; set; }
        public DateTime Timestamp { get; set; }
        public byte Score { get; set; }
        public string Content { get; set; }
    }
}