using System;
using Lacuna.Authentication.AspNetCore;
using Lacuna.Core.Data.Mapping;

namespace LD.Gateway.Models
{
    public class Account : IAuthenticatableIdentity
    {
        public virtual long Id { get; set; }

        public virtual DateTime CreationTimestamp { get; set; }
        public virtual DateTime LastActivityTimestamp { get; set; }

        [UnmappedColumn]
        public virtual string DisplayName => FirstName + " " + (Insertion + " ").Trim() + LastName;
        [UnmappedColumn]
        public virtual bool MaySignIn => true;

        [StringLength(100)]
        public virtual string FirstName { get; set; }
        [StringLength(100)]
        public virtual string Insertion { get; set; }
        [StringLength(100)]
        public virtual string LastName { get; set; }

        [StringLength(300)]
        public virtual string Email { get; set; }
        [StringLength(300)]
        public virtual string Password { get; set; }
    }
}