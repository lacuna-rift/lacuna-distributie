namespace LD.Gateway.Models
{
    public class ProductImage
    {
        public long Id { get; set; }
        public Product Product { get; set; }
        public byte[] Data { get; set; }
        public string Type { get; set; }
        public int Order { get; set; }
    }
}