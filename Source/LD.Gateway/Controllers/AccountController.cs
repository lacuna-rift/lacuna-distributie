﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Lacuna.Authentication.AspNetCore;
using Lacuna.Authentication.AspNetCore.Bootstrap;
using LD.Gateway.Models.Dtos;
using LD.Gateway.Models.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LD.Gateway.Controllers
{
    [ApiController]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        private readonly AccountAuthenticationLogic _accountAuthenticationLogic;

        public AccountController(AccountAuthenticationLogic accountAuthenticationLogic)
        {
            _accountAuthenticationLogic = accountAuthenticationLogic;
        }

        [HttpPost, Route("signup")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SignUp([FromBody] SignUpDto data)
        {
            if (string.IsNullOrEmpty(data?.Username) || string.IsNullOrEmpty(data?.Password))
                return NotFound($"Invalid credentials");
            
            await _accountAuthenticationLogic.CreateAuthenticatableIdentity(data);
            await _accountAuthenticationLogic.Commit();

            return Ok();
        }

        [HttpPost, Route("signin")]
        [ProducesResponseType(typeof(AccountDto), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(AccountDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SignIn([FromBody] SignInDto data)
        {
            if (string.IsNullOrEmpty(data?.Username) || string.IsNullOrEmpty(data?.Password))
                return NotFound($"Invalid credentials");
            
            var account = StubData.Accounts.SingleOrDefault(o => o.Email == data.Username);

            if(account == null)
                return NotFound($"Invalid credentials");

            return Ok(account);
        }
    }
}