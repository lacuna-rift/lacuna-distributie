﻿using System.Net;
using Lacuna.Authentication.AspNetCore;
using Lacuna.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace LD.Gateway.Controllers
{
    [ApiController]
    public class HealthcheckController : AuthenticatedController
    {
        private readonly IApplication _application;

        public HealthcheckController(IApplication application)
        {
            _application = application;
        }

        [HttpGet("api/healthcheck")]
        public IActionResult GetQuote()
        {
            if (!_application.EnvironmentInitialized)
                return StatusCode((int)HttpStatusCode.ServiceUnavailable);

            return Ok();
        }
    }
}