﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Lacuna.Core.Extensions;
using LD.Gateway.Models.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace LD.Gateway.Controllers
{
    [ApiController]
    [Route("api/shop")]
    public class ShopController : ControllerBase
    {

        [HttpGet, Route("categories/{slug}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ShopCategoryDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCategoryBySlug(string slug)
        {
            var category = StubData.Categories.SingleOrDefault(o => o.Slug == slug);

            if (category == null)
                return NotFound();

            return Ok(category);
        }

        [HttpGet, Route("categories")]
        [ProducesResponseType(typeof(ShopCategoryDto[]), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCategories()
        {
            var categories = StubData.Categories;
            return Ok(categories);
        }

        [HttpGet, Route("brands")]
        [ProducesResponseType(typeof(ProductBrandDto[]), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetBrands()
        {
            var brands = StubData.Brands;
            return Ok(brands);
        }

        [HttpGet, Route("products")]
        [ProducesResponseType(typeof(ProductsListDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProductsList(int page = 1, int limit = 10, string sort = "id-asc", string filters = "")
        {
            var products = StubData.Products.Skip((page - 1) * limit).Take(limit).ToArray();

            return Ok(new ProductsListDto()
            {
                Page = page,
                Pages = 1,
                Items = products,
                From = products.MinOrDefault(o => o.Id),
                To = products.MaxOrDefault(o => o.Id),
                Sort = sort,
                Total = StubData.Products.Length,
                Limit = limit,
                Filters = { }
            });
        }


        [HttpGet, Route("products/{slug}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProductDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProductBySlug(string slug)
        {
            var product = StubData.Products.SingleOrDefault(o => o.Slug == slug);

            if (product == null)
                return NotFound();

            return Ok(product);
        }

        [HttpGet, Route("reviews/{productId}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProductsListDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetProductReviews(int productId, int page = 1, int limit = 10, string sort = "id-asc", string filters = "")
        {
            if (StubData.Products.All(o => o.Id != productId))
                return NotFound($"Product with id [{productId}] not found");

            var reviews = StubData.Reviews.Where(o => o.ProductId == productId).Skip((page - 1) * limit).Take(limit).ToArray();

            return Ok(new ReviewsListDto()
            {
                Page = page,
                Pages = 1,
                Items = reviews,
                From = reviews.MinOrDefault(o => o.Id),
                To = reviews.MaxOrDefault(o => o.Id),
                Sort = sort,
                Total = StubData.Reviews.Count,
                Limit = limit,
            });
        }

        [HttpPost, Route("reviews/{productId}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProductsListDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AddProductReview(int productId, [FromBody] ReviewDto data)
        {
            if (StubData.Products.All(o => o.Id != productId))
                return NotFound($"Product with id [{productId}] not found");

            data.Date = DateTime.UtcNow;
            ;

            StubData.Reviews.Add(data);

            return Ok();
        }

    }
}