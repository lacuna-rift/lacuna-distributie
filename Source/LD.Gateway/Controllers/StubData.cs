﻿using System;
using System.Collections.Generic;
using System.Linq;
using LD.Gateway.Models.Dtos;

namespace LD.Gateway.Controllers
{
    public static class StubData
    {
        public static ShopCategoryDto[] Categories => new[] { new ShopCategoryDto { Id = 1, Name = "Electronics", Slug = "electronics", Image = "/images/missing.png" } };
        public static ProductBrandDto[] Brands => new[] { new ProductBrandDto { Id = 1, Name = "Philips", Slug = "philips", Image = "/images/missing.png", Country = "The Netherlands" } };
        public static ProductDto[] Products => new[]
        {
            new ProductDto
            {
                Id = 1,
                Name = "Philips One",
                Slug = "philips-one",
                Sku = "8710103751083-1", //ean = 8710103751083
                Images = new[] {"/images/missing.png"},
                Brand = Brands.Single(o => o.Name == "Philips"),
                Categories = Categories.Where(o => o.Name == "Electronics").ToArray(),
                Attributes = { },
                Badges = new[] {"new"},
                Tags = new[] {"shaver", "a-brand"},
                Price = 42.89m,
                NormalPrice = null,
                Description = "Super Sexy Description with <b>TAGS!<b>",
                Excerpt = "Super Sexy Description without tags",
                Rating = 4,
                Reviews = 77,
                Stock = ProductStock.InStock,
                Availability = null,
            },
            new ProductDto
            {
                Id = 2,
                Name = "Philips Two",
                Slug = "philips-two",
                Sku = "8710103751083-2", //ean = 8710103751083
                Images = new[] {"/assets/images/missing2.png"},
                Brand = Brands.Single(o => o.Name == "Philips"),
                Categories = Categories.Where(o => o.Name == "Electronics").ToArray(),
                Attributes = { },
                Badges = new[] {"new"},
                Tags = new[] {"shaver", "a-brand"},
                Price = 42.89m,
                NormalPrice = null,
                Description = "Super Sexy Description with <b>TAGS!<b>",
                Excerpt = "Super Sexy Description without tags",
                Rating = 4,
                Reviews = 77,
                Stock = ProductStock.InStock,
                Availability = null,
                Options = new[]{new ProductOptionsDto(){Name = "test",Slug = "slug",Type = "string"}},
                Type = new ProductTypeDto(){Name = "blah",Slug = "blahslug",AttributeGroups = {}}
            },
            new ProductDto
            {
                Id = 3,
                Name = "Philips Three",
                Slug = "philips-three",
                Sku = "8710103751083-3", //ean = 8710103751083
                Images = new[] {"/assets/images/missing3.png"},
                Brand = Brands.Single(o => o.Name == "Philips"),
                Categories = Categories.Where(o => o.Name == "Electronics").ToArray(),
                Attributes = { },
                Badges = new[] {"new"},
                Tags = new[] {"shaver", "a-brand"},
                Price = 42.89m,
                NormalPrice = null,
                Description = "Super Sexy Description with <b>TAGS!<b>",
                Excerpt = "Super Sexy Description without tags",
                Rating = 4,
                Reviews = 77,
                Stock = ProductStock.InStock,
                Availability = null,
                Options = new[]{new ProductOptionsDto(){Name = "test",Slug = "slug",Type = "string"}},
                Type = new ProductTypeDto(){Name = "blah",Slug = "blahslug",AttributeGroups = {}}
            },
            new ProductDto
            {
                Id = 4,
                Name = "Philips Four",
                Slug = "philips-four",
                Sku = "8710103751083-4", //ean = 8710103751083
                Images = new[] {"/assets/images/missing4.png"},
                Brand = Brands.Single(o => o.Name == "Philips"),
                Categories = Categories.Where(o => o.Name == "Electronics").ToArray(),
                Attributes = { },
                Badges = new[] {"new"},
                Tags = new[] {"shaver", "a-brand"},
                Price = 42.89m,
                NormalPrice = null,
                Description = "Super Sexy Description with <b>TAGS!<b>",
                Excerpt = "Super Sexy Description without tags",
                Rating = 4,
                Reviews = 77,
                Stock = ProductStock.InStock,
                Availability = null,
                Options = new[]{new ProductOptionsDto(){Name = "test",Slug = "slug",Type = "string"}},
                Type = new ProductTypeDto(){Name = "blah",Slug = "blahslug",AttributeGroups = {}}
            },
            new ProductDto
            {
                Id = 5,
                Name = "Philips Five",
                Slug = "philips-five",
                Sku = "8710103751083-5", //ean = 8710103751083
                Images = new[] {"/assets/images/missing5.png"},
                Brand = Brands.Single(o => o.Name == "Philips"),
                Categories = Categories.Where(o => o.Name == "Electronics").ToArray(),
                Attributes = { },
                Badges = new[] {"new"},
                Tags = new[] {"shaver", "a-brand"},
                Price = 42.89m,
                NormalPrice = null,
                Description = "Super Sexy Description with <b>TAGS!<b>",
                Excerpt = "Super Sexy Description without tags",
                Rating = 4,
                Reviews = 77,
                Stock = ProductStock.InStock,
                Availability = null,
                Options = new[]{new ProductOptionsDto(){Name = "test",Slug = "slug",Type = "string"}},
                Type = new ProductTypeDto(){Name = "blah",Slug = "blahslug",AttributeGroups = {}}
            }
        };

        public static List<ReviewDto> Reviews { get; } = new List<ReviewDto>
        {
            new ReviewDto
            {
                Id = 1,
                ProductId = 1,
                Date = DateTime.UtcNow,
                Rating = 4,
                Author = "Desiree",
                Avatar = "/assets/images/missing.png",
                Content = "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Integer consequat suscipit nisi ultricies vulputate. Donec laoreet orci nec egestas elementum.",
            }
        };

        public static List<AccountDto> Accounts { get; } = new List<AccountDto>
        {
            new AccountDto
            {
                Id = 1,
                FirstName = "Desiree",
                Insertion = "du",
                LastName = "Plessis",
                Email = "a@a.com"
            }
        };
    }
}