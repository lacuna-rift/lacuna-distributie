﻿using Lacuna.Core.Data.Persistance.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LD.Gateway.Controllers.Filters
{
    public class AutoCommitAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var unitOfWork = (IUnitOfWork)context.HttpContext.RequestServices.GetService(typeof(IUnitOfWork));

            unitOfWork.Commit();

            base.OnActionExecuted(context);
        }
    }
}