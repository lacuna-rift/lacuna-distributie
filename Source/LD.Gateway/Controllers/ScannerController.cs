﻿using System.Linq;
using System.Threading.Tasks;
using Lacuna.Authentication.AspNetCore;
using Lacuna.Core.Data.Persistance.Interfaces;
using LD.Source.Models;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;

namespace LD.Gateway.Controllers
{
    [ApiController]
    [Route("api/scanner")]
    public class ScannerController : AuthenticatedController
    {
        private readonly ISession _session;

        public ScannerController(ISession session)
        {
            _session = session;
        }

        [HttpGet("submit/condition")]
        public IActionResult SubmitCondition(string shipmentRef, string palletRef, string barcode, int externalPackaging, int internalPackaging, int product, int accessories)
        {
            return Ok();
        }

        [HttpGet("get/lpn/{lpnCode}")]
        public async Task<IActionResult> GetInfoByLpn(string lpnCode)
        {
            //var item = await _session.Query<ShipmentPalletItem>()
            //    .Where(o => o.Reference == lpnCode)
            //    .Where(o => o.ReferenceType == ItemReferenceCodeType.Amazon_LPN)
            //    .SingleOrDefaultAsync();

            //if (item == null)
            //    return NotFound();

            //var meta = await _session.Query<VendorItemMeta>()
            //    .Where(o => o.Id == item.Meta.Id)
            //    .SingleOrDefaultAsync();

            //if (meta == null)
            //    return NotFound();

            //return Ok(new
            //{
            //    Id = item.Id,
            //    ReportedCondition = item.ReportedCondition.ToString(),
            //    Meta = meta
            //});
            return Ok();
        }
    }
}