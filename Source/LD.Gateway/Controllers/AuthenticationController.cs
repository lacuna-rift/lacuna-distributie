﻿using Lacuna.Authentication.AspNetCore;
using Lacuna.Authentication.AspNetCore.Bootstrap;

namespace LD.Gateway.Controllers
{
    public class AuthenticationController : AuthenticationControllerBase
    {
        public AuthenticationController(IAuthenticationLogic authenticationLogic, AuthenticationCryptography authenticationCryptography)
            : base(authenticationLogic, authenticationCryptography)
        {
        }
    }
}
