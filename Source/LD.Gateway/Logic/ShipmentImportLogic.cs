using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lacuna.Core.Data.Persistance.Interfaces;
using LD.Source;
using LD.Source.Models;

namespace LD.Gateway.Logic
{
    public class ShipmentImportLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Dictionary<string, IManifestDatasource> _manifestDatasources;
        private readonly Dictionary<string, IManifestMetaDatasource> _metaDatasources;

        public ShipmentImportLogic(IUnitOfWork unitOfWork, IEnumerable<IManifestDatasource> manifestDatasources, IEnumerable<IManifestMetaDatasource> metaDatasources)
        {
            _unitOfWork = unitOfWork;
            _manifestDatasources = manifestDatasources.ToDictionary(o => o.Vendor);
            _metaDatasources = metaDatasources.ToDictionary(o => o.Vendor);
        }

        public void Import(string vendor, string reference)
        {
            return;
            if (_unitOfWork.Query<Shipment>().Any(o => o.Reference == reference)) return;

            var manifestDatasource = _manifestDatasources[vendor];
            var metaDatasource = _metaDatasources[vendor];

            var items = manifestDatasource.GetItems(reference);

            _unitOfWork.Insert(items[0].Pallet.Shipment);
            var knownPallets = new Dictionary<string, ShipmentPallet>();
            foreach (var item in items)
            {
                if (!knownPallets.TryGetValue(item.Pallet.Reference, out var pallet))
                {
                    _unitOfWork.Insert(item.Pallet);
                    pallet = knownPallets[item.Pallet.Reference] = item.Pallet;
                }
                item.Pallet = pallet;
            }

            var knownEans = _unitOfWork.Query<VendorItemMeta>().Where(o => o.Vendor == vendor).ToDictionary(o => o.EAN);
            for (var i = 0; i < items.Length; i++)
            {
                var item = items[i];
                //Console.Title = $"{i + 1} of {items.Length}  ({CachedWebClient.DownloadCount} / {CachedWebClient.CacheHits})";
                if (!knownEans.TryGetValue(item.Meta.EAN, out var meta))
                {
                    _unitOfWork.Insert(item.Meta);
                    foreach (var image in metaDatasource.GetImages(item.Meta))
                    {
                        _unitOfWork.Insert(image);
                    }

                    meta = knownEans[item.Meta.EAN] = item.Meta;
                }

                item.Meta = meta;
            }

            foreach (var item in items)
            {
                _unitOfWork.Insert(item);
            }
        }
    }
}