using System;
using System.Threading.Tasks;
using Lacuna.Core.Data.Persistance.Interfaces;

namespace LD.Gateway.Logic
{
    public class AccountAuthenticationLogic : IAuthenticationLogic
    {
        private readonly AccountAuthenticationRepository _accountAuthenticationRepository;
        private readonly AuthenticationCryptography _authenticationCryptography;
        private readonly IUnitOfWork _unitOfWork;

        public AccountAuthenticationLogic(AccountAuthenticationRepository accountAuthenticationRepository, AuthenticationCryptography authenticationCryptography, IUnitOfWork unitOfWork)
        {
            _accountAuthenticationRepository = accountAuthenticationRepository;
            _authenticationCryptography = authenticationCryptography;
            _unitOfWork = unitOfWork;
        }

        public Task CreateAuthenticatableIdentity(SignUpDto data)
        {
            var account = new Account
            {
                Email = data.Username,
                Password = _authenticationCryptography.HashHex(data.Password, HashAlgorithm.SHA256),
                CreationTimestamp = DateTime.UtcNow,
                LastActivityTimestamp = DateTime.UtcNow
            };

            return _accountAuthenticationRepository.CreateAuthenticatableIdentity(account);
        }

        public Task RecoverPassword(long identityId)
        {
            var secret = Guid.NewGuid();

            // update account
            _accountAuthenticationRepository.CreateSecret(identityId, secret.ToString());

            // send email

            return Task.CompletedTask;
        }

        public Task<IAuthenticatableIdentity> GetAuthenticatableIdentity(long identityId)
        {
            return Task.FromResult<IAuthenticatableIdentity>(_accountAuthenticationRepository.GetAuthenticatableIdentity(identityId));
        }

        public Task<IAuthenticatableIdentity> GetAuthenticatableIdentityByEmail(string email)
        {
            return Task.FromResult<IAuthenticatableIdentity>(_accountAuthenticationRepository.GetAuthenticatableIdentityByEmail(email));
        }

        public Task<IAuthenticatableIdentity> GetAuthenticatableIdentityBySecret(string secret)
        {
            return Task.FromResult<IAuthenticatableIdentity>(_accountAuthenticationRepository.GetAuthenticatableIdentityBySecret(secret));
        }

        public Task<IAuthenticatableIdentity> GetAuthenticatableIdentity(string username, string password)
        {
            var hashedPassword = _authenticationCryptography.HashHex(password, HashAlgorithm.SHA256);
            return Task.FromResult<IAuthenticatableIdentity>(_accountAuthenticationRepository.GetAuthenticatableIdentity(username, hashedPassword));
        }

        public Task UpdateLastActivity(long identityId)
        {
            _accountAuthenticationRepository.UpdateLastActivity(identityId);
            return Task.CompletedTask;
        }

        public Task Commit()
        {
            _unitOfWork.Commit();
            return Task.CompletedTask;
        }

        public Task UpdatePassword(long identityId, string password)
        {
            var newPassword = _authenticationCryptography.HashHex(password, HashAlgorithm.SHA256);
            _accountAuthenticationRepository.UpdatePassword(identityId, newPassword);
            return Task.CompletedTask;
        }
    }
}