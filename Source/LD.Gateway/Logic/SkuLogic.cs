﻿using System.Threading.Tasks;
using LD.Gateway.Models.Repositories;
using LD.Gateway.Models.V2;
using LD.Source.Models;

namespace LD.Gateway.Logic
{
    public class SkuLogic
    {
        private readonly StorageRepository _storageRepository;
        private readonly ShipmentRepository _shipmentRepository;

        public SkuLogic(StorageRepository storageRepository, ShipmentRepository shipmentRepository)
        {
            _storageRepository = storageRepository;
            _shipmentRepository = shipmentRepository;
        }

        public string GetSku(StorageSlot slot, ShipmentPalletItem item)
        {
            return $"I" +
                   $"-{slot.Warehouse:00}{slot.Zone:00}" +
                   $"-{slot.Rack:00}{slot.Row:00}{slot.Slot:00}" +
                   $"-{item.Id:###0}";
        }

        public Task<StorageSlot> GetStorageSlot(string sku)
        {
            var (_, warehouse, zone, rack, row, slot, _) = DecodeSku(sku);

            return _storageRepository.GetSlot(warehouse, zone, rack, row, slot);
        }

        public Task<ShipmentPalletItem> GetShipmentPalletItem(string sku)
        {
            var (type, _, _, _, _, _, itemId) = DecodeSku(sku);
           
            if (type == 'I')
            {
                return _shipmentRepository.GetItem(itemId);
            }

            return Task.FromResult<ShipmentPalletItem>(null);
        }

        private (char Type, byte Warehouse, byte Zone, byte Rack, byte Row, byte Slot, int ItemId) DecodeSku(string sku)
        {
            var type = char.ToUpper(sku[0]);
            var warehouse = byte.Parse(sku[2..4]);
            var zone = byte.Parse(sku[4..6]);
            var rack = byte.Parse(sku[7..9]);
            var row = byte.Parse(sku[9..11]);
            var slot = byte.Parse(sku[11..13]);
            var itemId = int.Parse(sku[14..]);
            return (type, warehouse, zone, rack, row, slot, itemId);
        }
    }
}