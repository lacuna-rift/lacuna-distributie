﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Lacuna.Core.Interfaces;
using LD.Gateway.Models.Dtos.Abstract;
using Newtonsoft.Json;
using NLog;

namespace LD.Gateway.Initializers
{
    public class ExportTypeScriptClassesInitializer : IEnvironmentInitializer
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public int Order { get; } = 10000;

        public void Initialize()
        {
            if (!Debugger.IsAttached) return;

            var sb = new StringBuilder();

            var types = GetType().Assembly.ExportedTypes.Where(TypeIsExportable).ToArray();
            Log.Debug($"Exporting {types.Length} types to LL.Portal typescript model definitions");
            foreach (var type in types)
            {
                BuildClass(type, sb);
                Log.Debug($" - {type.Name}");
            }

            var output = sb.ToString().Trim();

            File.WriteAllText("../LD.Frontend/projects/storefront/src/api/data.models.d.ts", output);
        }

        private void BuildClass(Type type, StringBuilder sb)
        {
            sb.AppendLine("");
            sb.AppendLine($"declare interface {type.Name} {{");
            foreach (var property in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy))
            {
                if (property.GetCustomAttribute<JsonIgnoreAttribute>() != null) continue;

                var name = property.GetCustomAttribute<JsonPropertyAttribute>()?.PropertyName;
                name ??= (char.ToLowerInvariant(property.Name[0]) + property.Name.Substring(1)).Replace("_", string.Empty);

                var typescriptType = GetTypescriptTypeName(property.PropertyType);

                sb.AppendLine($"    {name}: {typescriptType};");
            }
            sb.AppendLine("} ");
        }

        private bool TypeIsExportable(Type type)
        {
            return type.GetInterface(nameof(IExportedTypescriptDto)) != null;
        }

        private string GetTypescriptTypeName(Type type)
        {
            if (type.IsArray)
                return GetTypescriptTypeName(type.GetElementType()) + "[]";

            if (type == typeof(byte)) return "number";
            if (type == typeof(sbyte)) return "number";
            if (type == typeof(short)) return "number";
            if (type == typeof(ushort)) return "number";
            if (type == typeof(int)) return "number";
            if (type == typeof(uint)) return "number";
            if (type == typeof(long)) return "number";
            if (type == typeof(ulong)) return "number";
            if (type == typeof(float)) return "number";
            if (type == typeof(double)) return "number";
            if (type == typeof(decimal)) return "number";

            if (type == typeof(bool)) return "boolean";
            if (type == typeof(string)) return "string";
            if (type == typeof(DateTime)) return "Date";

            if (TypeIsExportable(type))
                return type.Name;

            throw new NotSupportedException($"Could not convert type [{type.Name}] to a typescript type");
        }
    }
}