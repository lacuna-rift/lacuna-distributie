﻿using System.Linq;
using Lacuna.Core.Data.Persistance.Interfaces;
using Lacuna.Core.Interfaces;
using LD.Gateway.Models;
using NLog;

namespace LD.Gateway.Initializers
{
    public class DevDataInitializer : IEnvironmentInitializer
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public int Order { get; } = 1;

        private readonly IUnitOfWork _unitOfWork;

        public DevDataInitializer(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Initialize()
        {
            if (!_unitOfWork.Query<Account>().Any())
            {
                _unitOfWork.Insert(new Account
                {
                    Email = "a@a.com",
                    Password = "abc",

                    FirstName = "",
                    Insertion = "",
                    LastName = "",
                });
            }

            _unitOfWork.Commit();
        }
    }
}