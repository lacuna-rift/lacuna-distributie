using LD.Source;
using LD.Source.Amazon;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace LD.Gateway
{
    public class Program
    {
        public const string ApplicationName = "LD.Gateway";

        public static void Main(string[] args)
        {
            CachedWebClient.RootCacheDirectory = "../../Artifacts/cache";
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>().UseUrls("https://+:8556");
                });
    }
}
