using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Lacuna.Authentication.AspNetCore;
using Lacuna.Core.Data;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Mapping;
using Lacuna.Core.Data.Persistance.Interfaces;
using Lacuna.Core.Extensions;
using Lacuna.Core.Interfaces;
using LD.Gateway.Initializers;
using LD.Gateway.Logic;
using LD.Gateway.Models;
using LD.Gateway.Models.Repositories;
using LD.Gateway.Models.V2;
using LD.Source;
using LD.Source.Amazon;
using LD.Source.Models;
using LD.Source.Models.Enums;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NHibernate.Linq;
using VendorItemImage = LD.Source.Models.VendorItemImage;

namespace LD.Gateway
{
    public class Startup
    {
        private const string DevOrigins = "_DevOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(DevOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200")
                            .AllowCredentials()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
            });

            ////////////////////////////////////////////////////////////////
            /////>  Modules
            ////////////////////////////////////////////////////////////////

            services.RegisterModule<AmazonSourceModule>();

            ////////////////////////////////////////////////////////////////
            /////>  ENUMS
            ////////////////////////////////////////////////////////////////

            services.RegisterAutoMappedEnum<ItemAccessoriesCondition>();
            services.RegisterAutoMappedEnum<ItemExternalPackagingCondition>();
            services.RegisterAutoMappedEnum<ItemInternalPackagingCondition>();
            services.RegisterAutoMappedEnum<ItemProductCondition>();
            services.RegisterAutoMappedEnum<ItemReportedCondition>();
            //services.RegisterAutoMappedEnum<ItemReferenceCodeType>();

            ////////////////////////////////////////////////////////////////
            /////>  TABLES
            ////////////////////////////////////////////////////////////////

            services.RegisterAutoMappedType<Account>();

            services.RegisterAutoMappedType<VendorItemMeta>();
            services.RegisterAutoMappedType<VendorItemImage>();
            services.RegisterAutoMappedType<Shipment>();
            services.RegisterAutoMappedType<ShipmentPallet>();
            services.RegisterAutoMappedType<ShipmentPalletItem>();
            services.RegisterAutoMappedType<StorageSlot>();

            ////////////////////////////////////////////////////////////////
            /////>  LOGIC
            ////////////////////////////////////////////////////////////////

            services.AddScoped<ShipmentImportLogic>();
            services.AddScoped<SkuLogic>();
            services.AddScoped<AccountAuthenticationLogic>();

            ////////////////////////////////////////////////////////////////
            /////>  REPOS
            ////////////////////////////////////////////////////////////////

            services.AddScoped<StorageRepository>();
            services.AddScoped<ShipmentRepository>();
            services.AddScoped<AccountAuthenticationRepository>();

            ////////////////////////////////////////////////////////////////

            services.AddScoped<IEnvironmentInitializer, DevDatabaseWipeInitializer>();
            services.AddScoped<IEnvironmentInitializer, DevDataInitializer>();
            services.AddScoped<IEnvironmentInitializer, ExportTypeScriptClassesInitializer>();

            services.RegisterAsMappingAssembly(GetType().Assembly);
            services.RegisterAsMigrationAssembly(GetType().Assembly);
            services.RegisterModule(new DataModule(Program.ApplicationName));

            services.AddResponseCompression();
            services.AddControllers();

            services.AddScoped<AuthenticationCryptography>();
            services.AddScoped<IAuthenticationLogic>(o => o.GetRequiredService<AccountAuthenticationLogic>());
            services.AddAuthentication(options =>
            {
                options.AddScheme<LRUserAuthenticationHandler>("LRAuthentication", "Lacuna Rift Authentication");
                options.DefaultScheme = "LRAuthentication";
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("LRAuthenticatedUser",
                    policy => policy.AddRequirements(new LRUserRequirement()));

                options.DefaultPolicy = options.GetPolicy("LRAuthenticatedUser");
            });
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.WriteIndented = true;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var application = app.ApplicationServices.GetService<IApplication>();
            application.Initialize(Program.ApplicationName, app.ApplicationServices);

            using (var scope = app.ApplicationServices.CreateScope())
            {
                scope.EnsureAllEnumsAreMappedToAppeaseTheDes();
                var services = scope.ServiceProvider.GetServices<IEnvironmentInitializer>();
                foreach (var initializer in services.OrderBy(o => o.Order))
                {
                    initializer.Initialize();
                }
            }
            // free up the memory claimed by debug stuff
            AutoLacunaClassMapMigrator.Clear();
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);


            if (env.IsDevelopment())
            {
                app.UseCors(DevOrigins);
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseResponseCompression();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            application.SetEnvironmentInitialized();

            foreach (var shipmentId in new[] { "AZPL_CR_BP1_20200727stl" })
            {
                using (var scope = app.ApplicationServices.CreateScope())
                {
                    var unitOfWork = scope.ServiceProvider.GetService<IUnitOfWork>();
                    var shipmentImportLogic = scope.ServiceProvider.GetService<ShipmentImportLogic>();

                    shipmentImportLogic.Import(AmazonConstants.Vendor, shipmentId);

                    unitOfWork.Commit();
                    continue;

                    var itemsByAsin = unitOfWork.Query<ShipmentPalletItem>()
                        .Fetch(o => o.Meta)
                        .Where(o => o.Pallet.Shipment.Reference == shipmentId)
                        .ToArray()
                        .GroupBy(o => o.Meta.ASIN)
                        .ToDictionary(o => o.Key, o => o.ToArray());

                    var asins = itemsByAsin.Select(o => o.Key).ToHashSet();
                    var imagesByAsin = unitOfWork.Query<VendorItemImage>()
                        .Fetch(o => o.Meta)
                        .Where(o => asins.Contains(o.Meta.ASIN))
                        .ToArray()
                        .GroupBy(o => o.Meta.ASIN)
                        .ToDictionary(o => o.Key, o => o.ToArray());

                    var dir = $"{CachedWebClient.RootCacheDirectory}/shipment/{shipmentId}___{itemsByAsin.Sum(o => o.Value.Length)}";
                    Directory.CreateDirectory(dir);
                    foreach (var kvp in itemsByAsin)
                    {
                        var images = imagesByAsin.GetValueOrDefault(kvp.Key) ?? new VendorItemImage[0];
                        var mimeType = images.FirstOrDefault(o => o.DisplayOrder == 0)?.MimeType ?? "png";
                        var data = images.FirstOrDefault(o => o.DisplayOrder == 0)?.Data ?? new byte[0];
                        File.WriteAllBytes($"{dir}/{kvp.Value[0].Meta.SuggestedRetailValue:###0}_{kvp.Value.Length}_{kvp.Value[0].Meta.ASIN}.{mimeType}", data);
                    }

                }
            }
        }
    }
}