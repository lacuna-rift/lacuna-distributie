﻿using LD.Source.Models;

namespace LD.Source
{
    public interface IManifestMetaDatasource
    {
        string Vendor { get; }

        VendorItemMeta GetMeta(string reference);
        VendorItemImage[] GetImages(VendorItemMeta meta);
    }
}