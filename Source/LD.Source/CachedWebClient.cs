﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;

namespace LD.Source
{
    public static class CachedWebClient
    {
        public static string RootCacheDirectory = "./cache";
        public static int DownloadCount = 0;
        public static int CacheHits = 0;

        public static string DownloadString(string url, Func<string, bool> retryPredicate = null)
        {
            var bytes = DownloadBytes(url);
            var str = Encoding.UTF8.GetString(bytes);
            if (retryPredicate?.Invoke(str) == true)
            {
                Thread.Sleep(5000);
                bytes = DownloadBytes(url, true);
                str = Encoding.UTF8.GetString(bytes);
                if (retryPredicate?.Invoke(str) == false)
                {

                }
            }
            return str;
        }
        public static byte[] DownloadBytes(string url, bool force = false)
        {
            var directory = $"{RootCacheDirectory}/html";
            Directory.CreateDirectory(directory);

            var file = url;
            foreach (var c in Path.GetInvalidFileNameChars())
                file = file.Replace(c.ToString(), ".");
            foreach (var c in Path.GetInvalidPathChars())
                file = file.Replace(c.ToString(), ".");

            file = $"{directory}/{file}";

            if (force || !File.Exists(file))
            {
                while (true)
                {
                    DownloadCount++;
                    var client = new HttpClient();
                    client.DefaultRequestHeaders.Add("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    client.DefaultRequestHeaders.Add("accept-encoding", "gzip, deflate, br");
                    client.DefaultRequestHeaders.Add("accept-language", "en,en-US;q=0.9,nl;q=0.8");
                    client.DefaultRequestHeaders.Add("cache-control", "max-age=0");
                    client.DefaultRequestHeaders.Add("dnt", "1");
                    client.DefaultRequestHeaders.Add("downlink", "10");
                    client.DefaultRequestHeaders.Add("ect", "4g");
                    client.DefaultRequestHeaders.Add("sec-fetch-dest", "document");
                    client.DefaultRequestHeaders.Add("sec-fetch-mode", "navigate");
                    client.DefaultRequestHeaders.Add("sec-fetch-site", "none");
                    client.DefaultRequestHeaders.Add("sec-fetch-user", "?1");
                    client.DefaultRequestHeaders.Add("upgrade-insecure-requests", "1");
                    client.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36");
                    var result = client.GetAsync(url).Result;
                    var data = result.Content.ReadAsByteArrayAsync().Result;
                    if (result.Content.Headers.ContentEncoding.FirstOrDefault() == "gzip")
                    {
                        data = Decompress(data);
                    }

                    if (result.StatusCode == HttpStatusCode.ServiceUnavailable)
                    {
                        Thread.Sleep(1000);
                        continue; // try again;
                    }


                    File.WriteAllBytes(file, data);
                    break;
                }
            }
            else
            {
                CacheHits++;
            }
            return File.ReadAllBytes(file);
        }



        public static byte[] Decompress(byte[] data)
        {
            using (var decompressedFileStream = new MemoryStream())
            {
                using (var originalFileStream = new MemoryStream(data))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }

                return decompressedFileStream.ToArray();
            }
        }
    }
}