﻿using System;
using Lacuna.Core.Data.Mapping;

namespace LD.Source.Models
{
    public class VendorItemImage
    {
        public virtual int Id { get; set; }

        [Reference]
        public virtual VendorItemMeta Meta { get; set; }

        [StringLength(2)]
        public virtual string LanguageCode { get; set; }

        public virtual DateTime Timestamp { get; set; }

        public virtual int DisplayOrder { get; set; }
        [ByteArrayLength(10 * 1024 * 1024)]
        public virtual byte[] Data { get; set; }
        [StringLength(20)]
        public virtual string MimeType { get; set; }
    }
}