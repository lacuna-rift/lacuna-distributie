﻿namespace LD.Source.Models.Enums
{
    public enum ItemProductCondition
    {
        /// <summary>
        /// Absolutely undamaged, aesthetically seems new from the factory
        /// </summary>
        Undamaged = 5,

        /// <summary>
        /// Clearly been unpacked before, but no apparent damage or usage
        /// </summary>
        LightDamage = 4,

        /// <summary>
        /// Light scratches, could have been tested and returned, but no further issues
        /// </summary>
        ModerateDamage = 3,

        /// <summary>
        /// Many scratches/dents does not seem like a new product, can only be sold as "used"
        /// </summary>
        HeavyDamage = 2,

        /// <summary>
        /// Object cannot be shipped as new or used, and should be refurbished, stripped or disposed of.
        /// </summary>
        Missing = 1,

        Unrated = 0
    }
}