﻿namespace LD.Source.Models.Enums
{
    public enum ItemReportedCondition
    {
        None = 0,

        Defective = 1,
        CarrierDamage = 2,
        WarehouseDamage = 3,
        CustomerDamage = 4,
        VendorDamage = 5,
        Expired = 6
    }
}