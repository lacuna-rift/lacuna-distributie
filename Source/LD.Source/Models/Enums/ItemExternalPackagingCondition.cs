﻿namespace LD.Source.Models.Enums
{
    public enum ItemExternalPackagingCondition
    {
        /// <summary>
        /// Absolutely undamaged, aesthetically seems new from the factory, or originally sold without packaging.
        /// </summary>
        Undamaged = 5,

        /// <summary>
        /// Light Scratches, Could be passed as "Handled in the warehouse" but seems new
        /// </summary>
        LightDamage = 4,

        /// <summary>
        /// Many scratches or dirt marks, Seems like its been shipped more than once, or previously opened
        /// </summary>
        ModerateDamage = 3,

        /// <summary>
        /// Not original packaging, Heavy Damage, Deep dents or very dirty, Too broken to be shipped in this package, must be replaced.
        /// </summary>
        HeavyDamage = 2,

        /// <summary>
        /// Has no external packaging
        /// </summary>
        Missing = 1,

        Unrated = 0
    }
}