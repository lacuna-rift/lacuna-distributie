﻿namespace LD.Source.Models.Enums
{
    public enum ItemInternalPackagingCondition
    {
        /// <summary>
        /// Absolutely undamaged, aesthetically seems new from the factory, or originally sold without packaging.
        /// </summary>
        Undamaged = 5,

        /// <summary>
        /// Styrofoam/cork has light dents, could be from shipping, All other material undamaged
        /// </summary>
        LightDamage = 4,

        /// <summary>
        /// Styrofoam/cork has mild damage or other packing material torn/damaged - Object seems like it may be damaged
        /// </summary>
        ModerateDamage = 3,

        /// <summary>
        /// Internal protective packing is too damaged and should be replaced - Object seems like it is probably damaged
        /// </summary>
        HeavyDamage = 2,

        /// <summary>
        /// Has no internal protective packaging
        /// </summary>
        Missing = 1,

        Unrated = 0
    }
}