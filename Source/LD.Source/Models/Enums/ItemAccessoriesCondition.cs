﻿namespace LD.Source.Models.Enums
{
    public enum ItemAccessoriesCondition
    {
        /// <summary>
        /// All present, undamaged and unused
        /// </summary>
        Undamaged = 5,

        /// <summary>
        /// All present, light damage, probably not used
        /// </summary>
        LightDamage = 4,

        /// <summary>
        /// All present, signs of use / dirty
        /// </summary>
        ModerateDamage = 3,

        /// <summary>
        /// Has missing parts/damaged parts which needs to be replaced or sold with partial accessories
        /// </summary>
        HeavyDamage = 2,

        /// <summary>
        /// All accessories missing/damaged, everything needs to be replaced or sold without accessories
        /// </summary>
        Missing = 1,

        Unrated = 0
    }
}