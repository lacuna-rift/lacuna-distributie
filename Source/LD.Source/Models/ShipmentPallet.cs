﻿using Lacuna.Core.Data.Mapping;

namespace LD.Source.Models
{
    public class ShipmentPallet
    {
        public virtual int Id { get; set; }
        
        [Reference]
        public virtual Shipment Shipment { get; set; }
        
        [StringLength(100)]
        public virtual string Reference { get; set; }
    }
}