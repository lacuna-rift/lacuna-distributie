﻿using System;
using Lacuna.Core.Data.Mapping;

namespace LD.Source.Models
{
    public class VendorItemMeta
    {
        public virtual int Id { get; set; }

        [StringLength(50)]
        public virtual string Vendor { get; set; }

        [StringLength(2)]
        public virtual string LanguageCode { get; set; }

        public virtual DateTime Timestamp { get; set; }

        [StringLength(100)]
        public virtual string Department { get; set; }
        [StringLength(100)]
        public virtual string Category { get; set; }
        [StringLength(100)]
        public virtual string SubCategory { get; set; }

        [Nullable, StringLength(10)] // Amazon Specific
        public virtual string ASIN { get; set; }
        [StringLength(13)]
        public virtual string EAN { get; set; }

        [DecimalSize(8, 2)]
        public virtual decimal SuggestedRetailValue { get; set; }

        [DecimalSize(8, 2)]
        public virtual decimal LastKnownPrice { get; set; }

        public virtual int Weight { get; set; } // in grams

        [StringLength(1000)]
        public virtual string Title { get; set; }
        [StringLength(2 * 8000)]
        public virtual string Description { get; set; }
        [Nullable,StringLength(2 * 8000)]
        public virtual string Features { get; set; }

        [StringLength(1000)]
        public virtual string LastSourceUrl { get; set; }
    }
}