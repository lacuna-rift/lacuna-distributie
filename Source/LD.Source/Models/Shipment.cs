﻿using Lacuna.Core.Data.Mapping;

namespace LD.Source.Models
{
    public class Shipment
    {
        public virtual int Id { get; set; }

        [StringLength(100)]
        public virtual string Reference { get; set; }

        [StringLength(10 * 1024 * 1024)] // 10MB limit
        public virtual string RawManifest { get; set; }
    }
}
