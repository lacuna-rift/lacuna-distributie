﻿using Lacuna.Core.Data.Mapping;
using LD.Source.Models.Enums;

namespace LD.Source.Models
{
    public class ShipmentPalletItem
    {
        public virtual int Id { get; set; }

        [Reference]
        public virtual ShipmentPallet Pallet { get; set; }

        [Reference]
        public virtual VendorItemMeta Meta { get; set; }

        [Nullable, StringLength(100)]
        public virtual string Reference { get; set; }

        [Nullable]
        public virtual ItemReferenceCodeType ReferenceType { get; set; }

        public virtual ItemReportedCondition ReportedCondition { get; set; }
        public virtual ItemExternalPackagingCondition ExternalPackagingCondition { get; set; }
        public virtual ItemInternalPackagingCondition InternalPackagingCondition { get; set; }
        public virtual ItemProductCondition ProductCondition { get; set; }
        public virtual ItemAccessoriesCondition AccessoriesCondition { get; set; }

        //public virtual ItemStatus Status { get; set; }
    }

    public enum ItemStatus
    {
        Disposed = -1,
        Expected = 0,
        Scanned = 1,
        Listed = 2,
        Sold = 3,
        Shipped = 4,
        Delivered = 5
    }

    public enum ItemReferenceCodeType
    {
        Default, // generic "who knows" code
        Amazon_FNSKU, // unique amazon item id
        Amazon_LPN,   // unique amazon return item id (has no relation to FNSKU)
    }

}