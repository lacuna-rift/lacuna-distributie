﻿using LD.Source.Models;

namespace LD.Source
{
    public interface IManifestDatasource
    {
        string Vendor { get; }

        ShipmentPalletItem[] GetItems(string shipmentRemoteId);
    }
}