﻿using System;

namespace LD.Tools.Testers.Keyboard
{
    internal class Program
    {
        private static void Main()
        {
            while (true)
            {
                var key = Console.ReadKey(true);
                Console.Beep();
                Console.WriteLine(key.Key.ToString() + "\t\t\t" + key.KeyChar);
            }

        }
    }
}
