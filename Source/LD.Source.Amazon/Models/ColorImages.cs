﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace LD.Source.Amazon.Models
{
    internal class ColorImages
    {

        [JsonProperty("initial")]
        public List<Initial> Initial { get; set; }
    }
}