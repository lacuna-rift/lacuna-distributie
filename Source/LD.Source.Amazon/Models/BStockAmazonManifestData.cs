﻿using Newtonsoft.Json;

namespace LD.Source.Amazon.Models
{
    internal class BStockAmazonManifestData
    {

        [JsonProperty("rows")]
        public string[][] Rows { get; set; }

        [JsonProperty("currency_code")]
        public string CurrencyCode { get; set; }

        [JsonProperty("original_extension")]
        public string OriginalExtension { get; set; }

    }
}