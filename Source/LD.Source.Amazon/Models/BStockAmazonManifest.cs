﻿using System.Linq;
using Newtonsoft.Json;

namespace LD.Source.Amazon.Models
{
    internal class BStockAmazonManifest
    {

        [JsonProperty("_index")]
        public string Index { get; set; }

        [JsonProperty("_type")]
        public string Type { get; set; }

        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("_version")]
        public int Version { get; set; }

        [JsonProperty("found")]
        public bool Found { get; set; }

        [JsonProperty("_source")]
        public BStockAmazonManifestData Source { get; set; }

        public string[] Headers => Source.Rows[0];
        public string[][] Rows => Source.Rows.Skip(1).ToArray();

    }
}