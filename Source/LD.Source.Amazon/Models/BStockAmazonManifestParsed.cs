﻿using System.ComponentModel;

namespace LD.Source.Amazon.Models
{
    internal class BStockAmazonManifestParsed
    {
        [Description("LiquidatorVendorCode")]
        public string LiquidatorVendorCode { get; set; }
        [Description("InventoryLocation")]
        public string InventoryLocation { get; set; }
        [Description("FC")]
        public string Fc { get; set; }
        [Description("IOG")]
        public string Iog { get; set; }
        [Description("CONDITION")]
        public string Condition { get; set; }
        [Description("ShipmentClosed")]
        public string ShipmentClosed { get; set; }
        [Description("BOL")]
        public string Bol { get; set; }
        [Description("Carrier")]
        public string Carrier { get; set; }
        [Description("ShipToCity")]
        public string ShipToCity { get; set; }
        [Description("RemovalOrderID")]
        public string RemovalOrderId { get; set; }
        [Description("ReturnID")]
        public string ReturnId { get; set; }
        [Description("ReturnItemID")]
        public string ReturnItemId { get; set; }
        [Description("ShipmentRequestID")]
        public string ShipmentRequestId { get; set; }
        [Description("PkgID")]
        public string PkgId { get; set; }
        [Description("GL")]
        public string Gl { get; set; }
        [Description("DEPARTMENT")]
        public string Department { get; set; }
        [Description("GL DESCRIPTION")]
        public string GlDescription { get; set; }
        [Description("Category Description")]
        public string CategoryDescription { get; set; }
        [Description("subcategory description")]
        public string SubCategoryDescription { get; set; }
        [Description("CategoryCode")]
        public string CategoryCode { get; set; }
        [Description("Category Code")]
        public string CategoryCode2 { set => CategoryCode = value; }
        [Description("CATEGORY")]
        public string Category { get; set; }
        [Description("SubcatCode")]
        public string SubcatCode { get; set; }
        [Description("Subcategory Code")]
        public string SubcatCode2 { set => SubcatCode = value; }
        [Description("SUBCATEGORY")]
        public string Subcategory { get; set; }
        [Description("ASIN")]
        public string Asin { get; set; }
        [Description("UPC")]
        public string Upc { get; set; }
        [Description("EAN")]
        public string Ean { get; set; }
        [Description("FCSku")]
        public string FcSku { get; set; }
        [Description("Item Desc")]
        public string ItemDesc { get; set; }
        [Description("QTY")]
        public string Qty { get; set; }
        [Description("ItemPkgWeight")]
        public string ItemPkgWeight { get; set; }
        [Description("ItemPkgWeightUOM")]
        public string ItemPkgWeightUom { get; set; }
        [Description("CURRENCY CODE")]
        public string CurrencyCode { get; set; }
        [Description("COST")]
        public string Cost { get; set; }
        [Description("TOTAL RETAIL")]
        public string TotalRetail { get; set; }
        [Description("TOTAL COST")]
        public string TotalCost { get; set; }
        [Description("a")]
        public string A { get; set; }
        [Description("RecoveryRateType")]
        public string RecoveryRateType { get; set; }
        [Description("AdjTotalRecovery")]
        public string AdjTotalRecovery { get; set; }
        [Description("AdjRecoveryRate")]
        public string AdjRecoveryRate { get; set; }
        [Description("AdjReason")]
        public string AdjReason { get; set; }
        [Description("FNSku")]
        public string FnSku { get; set; }
        [Description("LPN")]
        public string Lpn { get; set; }
        [Description("K&N palette ID")]
        public string KnPaletteId { get; set; } = "NONE";
        [Description("palett ID")]
        public string PalettId { get; set; }
        [Description("invoiced")]
        public string Invoiced { get; set; }
        [Description("shipped")]
        public string Shipped { get; set; }
        [Description("shipped month")]
        public string ShippedMonth { get; set; }
        [Description("comments")]
        public string Comments { get; set; }
        [Description("Status")]
        public string Status { get; set; }
        [Description("Remarks")]
        public string Remarks { get; set; }
        [Description("UNIT RETAIL")]
        public string UnitRetail { get; set; }
        [Description("inventory reference id")]
        public string InventoryReferenceId { get; set; }
        [Description("Warehouse")]
        public string Warehouse { get; set; }
        [Description("Listing_id")]
        public string ListingId { get; set; }
        [Description("slot_size")]
        public string SlotSize { get; set; }
        [Description("is_parcel")]
        public string IsParcel { get; set; }
        [Description("date_in")]
        public string DateIn { get; set; }
    }
}