﻿using Newtonsoft.Json;

namespace LD.Source.Amazon.Models
{
    internal class Initial
    {

        [JsonProperty("hiRes")]
        public string HiRes { get; set; }

        [JsonProperty("thumb")]
        public string Thumb { get; set; }

        [JsonProperty("large")]
        public string Large { get; set; }


        [JsonProperty("variant")]
        public string Variant { get; set; }

        [JsonProperty("lowRes")]
        public string LowRes { get; set; }

    }
}