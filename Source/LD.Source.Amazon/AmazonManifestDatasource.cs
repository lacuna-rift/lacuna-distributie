﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using LD.Source.Amazon.Models;
using LD.Source.Models;
using LD.Source.Models.Enums;
using Newtonsoft.Json;

namespace LD.Source.Amazon
{
    internal class AmazonManifestDatasource : IManifestDatasource
    {
        public string Vendor { get; } = AmazonConstants.Vendor;

        private readonly IManifestMetaDatasource _manifestMetaDatasource;

        public AmazonManifestDatasource(IManifestMetaDatasource manifestMetaDatasource)
        {
            _manifestMetaDatasource = manifestMetaDatasource;
        }

        public ShipmentPalletItem[] GetItems(string shipmentRemoteId)
        {
            return Get(shipmentRemoteId).ToArray();
        }

        public IEnumerable<ShipmentPalletItem> Get(string lotId)
        {
            var result = CachedWebClient.DownloadString($"https://m.bstock.com/m/search/direct/manifests/a2z/{lotId}", o => o.Contains("<title>503 Service Temporarily Unavailable</title>"));
            var data = JsonConvert.DeserializeObject<BStockAmazonManifest>(result);

            var shipment = new Shipment();
            shipment.Reference = lotId;
            shipment.RawManifest = result;

            var pallets = new Dictionary<string, ShipmentPallet>();
            var metas = new Dictionary<string, VendorItemMeta>();

            var mapping = typeof(BStockAmazonManifestParsed).GetProperties().ToDictionary(p => p.GetCustomAttribute<DescriptionAttribute>().Description.ToLower());
            for (var i = 0; i < data.Rows.Length; i++)
            {
                var row = data.Rows[i];
                var raw = new BStockAmazonManifestParsed();
                for (var j = 0; j < data.Headers.Length; j++)
                {
                    mapping[data.Headers[j].ToLower()].SetValue(raw, row[j]);
                }

                Console.Title = $"{lotId} = {i + 1}/{data.Rows.Length} ({CachedWebClient.DownloadCount} / {CachedWebClient.CacheHits})";
                for (int j = 0; j < int.Parse(raw.Qty); j++)
                {
                    yield return Map(shipment, pallets, raw, metas);
                }
            }
        }

        private ShipmentPalletItem Map(Shipment shipment, Dictionary<string, ShipmentPallet> pallets, BStockAmazonManifestParsed raw, Dictionary<string, VendorItemMeta> metas)
        {
            if (!pallets.TryGetValue(raw.KnPaletteId, out var pallet))
            {
                pallet = new ShipmentPallet { Reference = raw.PkgId, Shipment = shipment };
                pallets[pallet.Reference] = pallet;
            }

            if (!metas.TryGetValue(raw.Asin, out var meta))
            {
                meta = metas[raw.Asin] = _manifestMetaDatasource.GetMeta(raw.Asin);
                meta.EAN = raw.Ean.Split('.')[0];
                meta.ASIN = raw.Asin;
                meta.Department = raw.Department;
                meta.Category = raw.Category;
                meta.SubCategory = raw.Subcategory;
                meta.Weight = (int)decimal.Parse(string.IsNullOrWhiteSpace(raw.ItemPkgWeight) ? "0" : raw.ItemPkgWeight);
                meta.SuggestedRetailValue = decimal.Parse(raw.UnitRetail);
            }

            return new ShipmentPalletItem
            {
                Pallet = pallet,
                Reference = raw.Lpn,
                ReferenceType = ItemReferenceCodeType.Amazon_LPN,
                ReportedCondition = GetReportedCondition(raw.Condition),
                Meta = meta,
            };
        }

        private ItemReportedCondition GetReportedCondition(string rawCondition)
        {
            switch (rawCondition)
            {
                case "Defective": return ItemReportedCondition.Defective;
                case "Carrier Damage": return ItemReportedCondition.CarrierDamage;
                case "Customer Damage": return ItemReportedCondition.CustomerDamage;
                case "Vendor Damage": return ItemReportedCondition.VendorDamage;
                case "Warehouse Damage": return ItemReportedCondition.WarehouseDamage;
                case "Expired": return ItemReportedCondition.Expired;
            }
            throw new NotImplementedException();
        }
    }
}
