﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using LD.Source.Amazon.Models;
using LD.Source.Models;
using Newtonsoft.Json;
using NLog;

namespace LD.Source.Amazon
{
    internal class AmazonManifestMetaDatasource : IManifestMetaDatasource
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public string Vendor { get; } = AmazonConstants.Vendor;

        public VendorItemMeta GetMeta(string reference)
        {
            Log.Info($"Downloading meta for : {reference}");
            var url = $"https://www.amazon.de/_/dp/{reference}/?language=en_GB";

            var retriesLeft = 3;
            while (retriesLeft-- > 0)
            {
                try
                {
                    var html = CachedWebClient.DownloadString(url);
                    var doc = new HtmlDocument();
                    doc.LoadHtml(html);

                    var titleNode = doc.DocumentNode.SelectSingleNode("//html/head/title")?.InnerText.Trim();
                    var descriptionNode = doc.GetElementbyId("productDescription")?.InnerText.Trim();
                    var featuresNode = doc.GetElementbyId("feature-bullets")?.Descendants().Where(o => o.Name == "li" && o.Id != "replacementPartsFitmentBullet")?.Select(o => WebUtility.HtmlDecode(o.InnerText == null ? "" : $"* {o.InnerText.Trim()}"));

                    var priceBlockText = doc.GetElementbyId("priceblock_ourprice")?.InnerText.Split('-')[^1].Trim().Substring(1).Trim();

                    var priceDec = decimal.Parse(priceBlockText ?? "0");

                    return new VendorItemMeta
                    {
                        Title = WebUtility.HtmlDecode(titleNode ?? "Unknown").Split(": Amazon.")[0],
                        Description = LimitTo(descriptionNode ?? "", 16000),
                        Features = string.Join("\n", featuresNode ?? new List<string>()),
                        LanguageCode = "EN",
                        LastKnownPrice = priceDec,
                        LastSourceUrl = url,
                        Vendor = Vendor,
                        Timestamp = DateTime.UtcNow
                    };
                }
                catch (Exception e)
                {
                    Log.Error(e);
                    Thread.Sleep(5000);
                }
            }

            Log.Error($"Could not load resource [{Vendor} Metadata for item {reference}]");

            return new VendorItemMeta
            {
                Title = "Not Found",
                Description = "Not Found",
                LanguageCode = "EN",
                LastKnownPrice = 0,
                LastSourceUrl = url,
                Vendor = Vendor,
                Timestamp = DateTime.UtcNow,
            };
        }

        private string LimitTo(string text, int length)
        {
            if (text.Length < length)
                return text;
            return text.Substring(0, length);
        }

        public VendorItemImage[] GetImages(VendorItemMeta meta)
        {
            Log.Info($"Downloading images for : {meta.Id} / {meta.ASIN}");


            var retriesLeft = 3;
            while (retriesLeft-- > 0)
            {
                var images = new List<VendorItemImage>();
                try
                {
                    var url = $"https://www.amazon.de/_/dp/{meta.ASIN}/?language=en_GB";

                    var html = CachedWebClient.DownloadString(url);
                    var doc = new HtmlDocument();
                    doc.LoadHtml(html);

                    var innerText = doc.DocumentNode.Descendants().SingleOrDefault(o => o.Name == "script" && o.InnerText.Contains("ImageBlockATF"))?.InnerText;
                    if (innerText != null)
                    {
                        var split = innerText.Split("var data =");
                        var split2 = split[1].Split(";");

                        var airyConfigIndex = split2[0].IndexOf("airyConfig") - 1;
                        var subJson = split2[0].Substring(0, airyConfigIndex).Replace("\n", "");
                        var subJson2 = subJson.Substring(0, subJson.Length - 1) + "}";

                        var colorImages = JsonConvert.DeserializeAnonymousType(subJson2, new { ColorImages = new ColorImages() });
                        foreach (var img in colorImages.ColorImages.Initial)
                        {
                            //data
                            var data = img.HiRes ?? img.Large ?? img.LowRes;

                            //order
                            int order;
                            if (img.Variant.ToLower().Equals("main"))
                                order = 0;
                            else
                                int.TryParse(img.Variant.Replace("PT", ""), out order);

                            //mimetypes
                            var splitFileName = data?.Split('.');

                            var item = new VendorItemImage();
                            item.Data = data == null ? new byte[0] : CachedWebClient.DownloadBytes(data);
                            item.DisplayOrder = order;
                            item.Timestamp = DateTime.UtcNow;
                            item.LanguageCode = "EN";
                            item.MimeType = splitFileName == null ? "png" : splitFileName[^1];
                            item.Meta = meta;
                            images.Add(item);
                        }
                    }
                    else
                    {

                    }

                    return images.ToArray();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                    Thread.Sleep(5000);
                }
            }
            Log.Error($"Could not load resource [{Vendor} Metadata Images for item {meta.Id}/{meta.ASIN}]");
            return new VendorItemImage[0];
        }
    }
}