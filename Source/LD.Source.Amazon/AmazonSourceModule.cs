﻿using Lacuna.Core;
using Microsoft.Extensions.DependencyInjection;

namespace LD.Source.Amazon
{
    public class AmazonSourceModule : LoadOnceModule
    {
        protected override void LoadOnce(IServiceCollection builder)
        {
            builder.AddScoped<IManifestDatasource, AmazonManifestDatasource>();
            builder.AddScoped<IManifestMetaDatasource, AmazonManifestMetaDatasource>();
        }
    }
}
