﻿namespace LD.Source.Amazon
{
    public static class AmazonConstants
    {
        public const string Vendor = "Amazon";
        public static readonly string[] Languages = { "EN", "DE", "NL" };
    }
}
